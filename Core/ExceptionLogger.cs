﻿using log4net;
using System;

namespace Plus.Core
{
    public static class ExceptionLogger
    {
        private static readonly ILog _exceptionLogger = LogManager.GetLogger(typeof(ExceptionLogger));

        public static void LogQueryError(string query, Exception exception)
        {
            _exceptionLogger.Error("Error in query:\r\n" + query + "\r\n" + exception + "\r\n\r\n");
        }

        public static void LogException(Exception exception)
        {
            _exceptionLogger.ErrorFormat("Exception:\r\n" + exception + "\r\n\r\n");
        }

        public static void LogCriticalException(Exception exception)
        {
            _exceptionLogger.Error("Critical Exception:\r\n" + exception + "\r\n\r\n");
        }

        public static void LogThreadException(Exception exception)
        {
            _exceptionLogger.Error("Thread Exception:\r\n" + exception + "\r\n\r\n");
        }

        public static void LogWiredException(Exception exception)
        {
            _exceptionLogger.Error("Wired Exception:\r\n" + exception + "\r\n\r\n");
        }
    }
}
