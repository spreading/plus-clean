﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Core.FigureData.Types
{
    public class Color
    {
        public int Id { get; }
        public int Index { get; }
        public int ClubLevel { get; }
        public bool Selectable { get; }
        public string Value { get; }

        public Color(int id, int index, int clubLevel, bool selectable, string value)
        {
            this.Id = id;
            this.Index = index;
            this.ClubLevel = clubLevel;
            this.Selectable = selectable;
            this.Value = value;
        }
    }
}
