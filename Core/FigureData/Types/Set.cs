﻿using System.Collections.Generic;

namespace Plus.Core.FigureData.Types
{
    class Set
    {
        public int Id { get; }
        public string Gender { get; }
        public int ClubLevel { get; }
        public bool Colorable { get; }
        public bool Selectable { get; }
        public bool Preselectable { get; }

        private Dictionary<string, Part> _parts;

        public Set(int id, string gender, int clubLevel, bool colorable, bool selectable, bool preselectable)
        {
            this.Id = id;
            this.Gender = gender;
            this.ClubLevel = clubLevel;
            this.Colorable = colorable;
            this.Selectable = selectable;
            this.Preselectable = preselectable;

            this._parts = new Dictionary<string, Part>();
        }

        public Dictionary<string, Part> Parts
        {
            get { return this._parts; }
            set { this._parts = value; }
        }
    }
}
