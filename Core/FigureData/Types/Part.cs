﻿namespace Plus.Core.FigureData.Types
{
    class Part
    {
        public int Id { get; }
        public SetType SetType { get; }
        public bool Colorable { get; }
        public int Index { get; }
        public int ColorIndex { get; }


        public Part(int id, SetType setType, bool colorable, int index, int colorIndex)
        {
            this.Id = id;
            this.SetType = setType;
            this.Colorable = colorable;
            this.Index = index;
            this.ColorIndex = colorIndex;
        }
    }
}
