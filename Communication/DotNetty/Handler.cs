﻿using DotNetty.Transport.Channels;
using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.GameClients;

namespace Plus.Communication.DotNetty
{
    public class Handler : SimpleChannelInboundHandler<ClientPacket>
    {
        public override void ChannelRegistered(IChannelHandlerContext context)
        {
            PlusEnvironment.Game.ClientManager.CreateAndStartClient(context);
            base.ChannelActive(context);
        }

        public override void ChannelUnregistered(IChannelHandlerContext context)
        {
            PlusEnvironment.Game.ClientManager.DisposeConnection(context.Channel.Id);
            base.ChannelUnregistered(context);
        }

        public override void ChannelReadComplete(IChannelHandlerContext context)
        {
            context.Flush();
            base.ChannelReadComplete(context);
        }

        protected override void ChannelRead0(IChannelHandlerContext ctx, ClientPacket msg)
        {
            if (PlusEnvironment.Game.ClientManager.TryGetClient(ctx.Channel.Id, out GameClient client))
            {
                PlusEnvironment.Game.PacketManager.TryExecutePacket(client, msg);
            }
        }
    }
}