﻿using DotNetty.Codecs;
using Plus.Communication.Packets.Outgoing;
using DotNetty.Transport.Channels;
using System.Collections.Generic;
using System;

namespace Plus.Communication.DotNetty.Codec
{
    public class Encoder : MessageToMessageEncoder<ServerPacket>
    {
        protected override void Encode(IChannelHandlerContext context, ServerPacket message, List<Object> output)
        {
            if (!message.HasLength)
            {
                message.Buffer.SetInt(0, message.Buffer.WriterIndex - 4);
            }

            output.Add(message.Buffer);
        }
    }
}