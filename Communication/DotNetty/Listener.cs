﻿using DotNetty.Buffers;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using log4net;
using Plus.Communication.DotNetty.Codec;

namespace Plus.Communication.DotNetty
{
    public class Listener
    {
        private readonly ILog logger = LogManager.GetLogger(typeof(Listener));

        public Listener(int port)
        {
            IEventLoopGroup bossGroup = new MultithreadEventLoopGroup(1);
            IEventLoopGroup workerGroup = new MultithreadEventLoopGroup(10);

            ServerBootstrap server = new ServerBootstrap()
                .Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new ActionChannelInitializer<IChannel>(channel =>
                {
                    channel.Pipeline.AddLast("packetEncoder", new Encoder());
                    channel.Pipeline.AddLast("packetDecoder", new Decoder());
                    channel.Pipeline.AddLast("clientHandler", new Handler());
                }))
                .ChildOption(ChannelOption.TcpNodelay, true)
                .ChildOption(ChannelOption.SoKeepalive, true)
                .ChildOption(ChannelOption.SoRcvbuf, 1024)
                .ChildOption(ChannelOption.RcvbufAllocator, new FixedRecvByteBufAllocator(1024))
                .ChildOption(ChannelOption.Allocator, UnpooledByteBufferAllocator.Default);
                logger.Debug($"Server is listening on port: {port}");
            server.BindAsync(port);
        }
    }
}
