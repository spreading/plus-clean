﻿
using MySql.Data.MySqlClient;
using System;

namespace Plus.Communication.Packets.Outgoing.Moderation
{
    class ModeratorUserInfoComposer : ServerPacket
    {
        public ModeratorUserInfoComposer(MySqlDataReader User, MySqlDataReader Info)
            : base(ServerPacketHeader.ModeratorUserInfoMessageComposer)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Info.GetDouble("trading_locked"));


            base.WriteInteger(User != null ? User.GetInt32("id") : 0);
           base.WriteString(User != null ? User.GetString("username") : "Unknown");
           base.WriteString(User != null ? User.GetString("look") : "Unknown");
            base.WriteInteger(User != null ? Convert.ToInt32(Math.Ceiling((PlusEnvironment.GetUnixTimestamp() - User.GetDouble("account_created")) / 60)) : 0);
            base.WriteInteger(User != null ? Convert.ToInt32(Math.Ceiling((PlusEnvironment.GetUnixTimestamp() - User.GetDouble("last_online")) / 60)) : 0);
            base.WriteBoolean(User != null ? PlusEnvironment.Game.ClientManager.GetClientByUserID(User.GetInt32("id")) != null : false);
            base.WriteInteger(Info != null ? Info.GetInt32("cfhs") : 0);
            base.WriteInteger(Info != null ? Info.GetInt32("cfhs_abusive") : 0);
            base.WriteInteger(Info != null ? Info.GetInt32("cautions") : 0);
            base.WriteInteger(Info != null ? Info.GetInt32("bans") : 0);
            base.WriteInteger(Info != null ? Info.GetInt32("trading_locks_count") : 0);//Trading lock counts
           base.WriteString(Info.GetDouble("trading_locked") != 0 ? (origin.ToString("dd/MM/yyyy HH:mm:ss")) : "0");//Trading lock
           base.WriteString("");//Purchases
            base.WriteInteger(0);//Itendity information tool
            base.WriteInteger(0);//Id bans.
           base.WriteString(User != null ? User.GetString("mail") : "Unknown");
           base.WriteString("");//user_classification
        }
    }
}