﻿using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;

namespace Plus.Communication.Packets.Outgoing.Avatar
{
    class WardrobeComposer : ServerPacket
    {
        public WardrobeComposer(GameClient Session)
            : base(ServerPacketHeader.WardrobeMessageComposer)
        {
            base.WriteInteger(1);
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `slot_id`,`look`,`gender` FROM `user_wardrobe` WHERE `user_id` = '" + Session.GetHabbo().Id + "'");
                using (var reader = dbClient.ExecuteReader())
                    if (reader.HasRows)
                    {
                        dbClient.SetQuery("SELECT COUNT(*) AS amount FROM user_wardrobe WHERE `user_id` = '" + Session.GetHabbo().Id + "'");
                        using (var newReader = dbClient.ExecuteReader())
                            if (newReader.Read())
                                base.WriteInteger(newReader.GetInt32("amount"));

                        base.WriteInteger(reader.GetInt32("slot_id"));
                        base.WriteString(reader.GetString("look"));
                        base.WriteString(reader.GetString("gender").ToUpper());
                    }
                    else
                        base.WriteInteger(0);
            }
        }
    }
}