﻿using System;
using Plus.Database.Interfaces;

namespace Plus.Communication.Packets.Outgoing.Marketplace
{
    class MarketPlaceOwnOffersComposer : ServerPacket
    {
        public MarketPlaceOwnOffersComposer(int UserId)
            : base(ServerPacketHeader.MarketPlaceOwnOffersMessageComposer)
        {
            int i = 0;
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT SUM(asking_price) FROM catalog_marketplace_offers WHERE state = '2' AND user_id = '" + UserId + "'");
                i = dbClient.GetInteger();
                base.WriteInteger(i);

                dbClient.SetQuery("SELECT COUNT(*) AS amount FROM catalog_marketplace_offers WHERE user_id = '" + UserId + "'");
                using (var reader = dbClient.ExecuteReader())
                    if (reader.Read())
                        base.WriteInteger(reader.GetInt32("amount"));

                dbClient.SetQuery("SELECT timestamp, state, offer_id, item_type, sprite_id, total_price, limited_number, limited_stack FROM catalog_marketplace_offers WHERE user_id = '" + UserId + "'");
                using (var reader = dbClient.ExecuteReader())
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int num2 = Convert.ToInt32(Math.Floor((reader.GetDouble("timestamp") + 172800.0) - PlusEnvironment.GetUnixTimestamp()) / 60.0);
                            int num3 = int.Parse(reader.GetString("state"));
                            if ((num2 <= 0) && (num3 != 2))
                            {
                                num3 = 3;
                                num2 = 0;
                            }
                            base.WriteInteger(reader.GetInt32("offer_id"));
                            base.WriteInteger(num3);
                            base.WriteInteger(1);
                            base.WriteInteger(reader.GetInt32("sprite_id"));

                            base.WriteInteger(256);
                            base.WriteString("");
                            base.WriteInteger(reader.GetInt32("limited_number"));
                            base.WriteInteger(reader.GetInt32("limited_stack"));

                            base.WriteInteger(reader.GetInt32("total_price"));
                            base.WriteInteger(num2);
                            base.WriteInteger(reader.GetInt32("sprite_id"));
                        }
                    }
                    else
                    {
                        base.WriteInteger(0);
                    }
            }
        }
    }
}