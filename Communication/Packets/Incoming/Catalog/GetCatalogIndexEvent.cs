﻿using System;
using Plus.Communication.Packets.Incoming;

using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.Communication.Packets.Outgoing.BuildersClub;

namespace Plus.Communication.Packets.Incoming.Catalog
{
    public class GetCatalogIndexEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            /*int Sub = 0;

            if (Session.GetHabbo().SubscriptionManager.HasSubscription)
            {
                Sub = Session.GetHabbo().SubscriptionManager.GetSubscription().SubscriptionId;
            }*/

            Session.SendPacket(new CatalogIndexComposer(Session, PlusEnvironment.Game.CatalogManager.GetPages()));//, Sub));
            Session.SendPacket(new CatalogItemDiscountComposer());
            Session.SendPacket(new BCBorrowedItemsComposer());
        }
    }
}