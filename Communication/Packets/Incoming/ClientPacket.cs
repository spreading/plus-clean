﻿using System.Text;
using DotNetty.Buffers;

namespace Plus.Communication.Packets.Incoming
{
    public class ClientPacket
    {
        private IByteBuffer buffer;
        public short Header { get; }

        public ClientPacket(IByteBuffer buf)
        {
            buffer = buf;
            Header = buffer.ReadShort();
        }

        public string PopString()
        {
            int length = buffer.ReadShort();
            IByteBuffer data = buffer.ReadBytes(length);
            return Encoding.Default.GetString(data.Array);
        }

        public int PopInt() =>
            buffer.ReadInt();

        public bool PopBoolean() =>
            buffer.ReadByte() == 1;
    }
}