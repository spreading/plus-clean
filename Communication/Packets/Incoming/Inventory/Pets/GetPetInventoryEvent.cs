﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms.AI;
using Plus.Communication.Packets.Outgoing.Inventory.Pets;

namespace Plus.Communication.Packets.Incoming.Inventory.Pets
{
    class GetPetInventoryEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session.GetHabbo().InventoryComponent == null)
                return;

            ICollection<Pet> Pets = Session.GetHabbo().InventoryComponent.GetPets();
            Session.SendPacket(new PetInventoryComposer(Pets));
        }
    }
}