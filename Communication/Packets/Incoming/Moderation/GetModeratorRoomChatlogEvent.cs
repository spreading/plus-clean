﻿using System.Collections.Generic;
using Plus.HabboHotel.Rooms;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Users;
using Plus.HabboHotel.Rooms.Chat.Logs;
using Plus.Communication.Packets.Outgoing.Moderation;

namespace Plus.Communication.Packets.Incoming.Moderation
{
    class GetModeratorRoomChatlogEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            if (!Session.GetHabbo().Permissions.HasRight("mod_tool"))
                return;

            int Junk = Packet.PopInt();
            int RoomId = Packet.PopInt();

            if (!PlusEnvironment.Game.RoomManager.TryGetRoom(RoomId, out Room Room))
            {
                return;
            }

            PlusEnvironment.Game.ChatManager.GetLogs().FlushAndSave();

            List<ChatlogEntry> Chats = new List<ChatlogEntry>();
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `chatlogs` WHERE `room_id` = @id ORDER BY `id` DESC LIMIT 100");
                dbClient.AddParameter("id", RoomId);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        Habbo Habbo = PlusEnvironment.GetHabboById(reader.GetInt32("user_id"));

                        if (Habbo != null)
                        {
                            Chats.Add(new ChatlogEntry(reader.GetInt32("user_id"), RoomId, reader.GetString("message"), reader.GetDouble("timestamp"), Habbo));
                        }
                    }
            }

            Session.SendPacket(new ModeratorRoomChatlogComposer(Room, Chats));
        }
    }
}