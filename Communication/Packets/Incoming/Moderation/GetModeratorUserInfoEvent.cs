﻿using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.Database.Interfaces;
using MySql.Data.MySqlClient;

namespace Plus.Communication.Packets.Incoming.Moderation
{
    class GetModeratorUserInfoEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (!Session.GetHabbo().Permissions.HasRight("mod_tool"))
                return;

            int UserId = Packet.PopInt();

            MySqlDataReader User = null;
            MySqlDataReader Info = null;
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`username`,`online`,`mail`,`ip_last`,`look`,`account_created`,`last_online` FROM `users` WHERE `id` = '" + UserId + "' LIMIT 1");
                using (var reader = dbClient.ExecuteReader())
                    if (!reader.HasRows)
                    {
                        Session.SendNotification(PlusEnvironment.LanguageManager.TryGetValue("user.not_found"));
                        return;
                    }
                    else
                        User = reader;

                dbClient.SetQuery("SELECT `cfhs`,`cfhs_abusive`,`cautions`,`bans`,`trading_locked`,`trading_locks_count` FROM `user_info` WHERE `user_id` = '" + UserId + "' LIMIT 1");
                using (var reader = dbClient.ExecuteReader())
                    if (!reader.HasRows)
                    {
                        dbClient.RunQuery("INSERT INTO `user_info` (`user_id`) VALUES ('" + UserId + "')");
                        dbClient.SetQuery("SELECT `cfhs`,`cfhs_abusive`,`cautions`,`bans`,`trading_locked`,`trading_locks_count` FROM `user_info` WHERE `user_id` = '" + UserId + "' LIMIT 1");
                        using (var newReader = dbClient.ExecuteReader())
                            if (reader.Read())
                                Info = reader;
                    }
            }


            Session.SendPacket(new ModeratorUserInfoComposer(User, Info));
        }
    }
}