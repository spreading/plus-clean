﻿using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.HabboHotel.Rooms.Chat.Logs;
using Plus.Database.Interfaces;
using Plus.Utilities;
using Plus.HabboHotel.Rooms;

namespace Plus.Communication.Packets.Incoming.Moderation
{
    class GetModeratorUserChatlogEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            if (!Session.GetHabbo().Permissions.HasRight("mod_tool"))
                return;

            Habbo Data = PlusEnvironment.GetHabboById(Packet.PopInt());
            if (Data == null)
            {
                Session.SendNotification("Unable to load info for user.");
                return;
            }

            PlusEnvironment.Game.ChatManager.GetLogs().FlushAndSave();

            List<KeyValuePair<RoomData, List<ChatlogEntry>>> Chatlogs = new List<KeyValuePair<RoomData, List<ChatlogEntry>>>();
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `room_id`,`entry_timestamp`,`exit_timestamp` FROM `user_roomvisits` WHERE `user_id` = '" + Data.Id + "' ORDER BY `entry_timestamp` DESC LIMIT 7");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        RoomData RoomData = PlusEnvironment.Game.RoomManager.GenerateRoomData(reader.GetInt32("room_id"));
                        if (RoomData == null)
                        {
                            continue;
                        }

                        double TimestampExit = (reader.GetDouble("exit_timestamp") <= 0 ? UnixTimestamp.GetNow() : reader.GetDouble("exit_timestamp"));

                        Chatlogs.Add(new KeyValuePair<RoomData, List<ChatlogEntry>>(RoomData, GetChatlogs(RoomData, reader.GetDouble("entry_timestamp"), TimestampExit)));
                    }

                Session.SendPacket(new ModeratorUserChatlogComposer(Data, Chatlogs));
            }
        }

        private List<ChatlogEntry> GetChatlogs(RoomData RoomData, double TimeEnter, double TimeExit)
        {
            List<ChatlogEntry> Chats = new List<ChatlogEntry>();
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `user_id`, `timestamp`, `message` FROM `chatlogs` WHERE `room_id` = " + RoomData.Id + " AND `timestamp` > " + TimeEnter + " AND `timestamp` < " + TimeExit + " ORDER BY `timestamp` DESC LIMIT 100");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        Habbo Habbo = PlusEnvironment.GetHabboById(reader.GetInt32("user_id"));

                        if (Habbo != null)
                        {
                            Chats.Add(new ChatlogEntry(reader.GetInt32("user_id"), RoomData.Id, reader.GetString("message"), reader.GetDouble("timestamp"), Habbo));
                        }
                    }
            }

            return Chats;
        }
    }
}
