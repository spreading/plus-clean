﻿using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.HabboHotel.Moderation;

namespace Plus.Communication.Packets.Incoming.Moderation
{
    class ReleaseTicketEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null || !Session.GetHabbo().Permissions.HasRight("mod_tool"))
                return;

            int Amount = Packet.PopInt();

            for (int i = 0; i < Amount; i++)
            {
                if (!PlusEnvironment.Game.ModerationManager.TryGetTicket(Packet.PopInt(), out ModerationTicket Ticket))
                    continue;

                Ticket.Moderator = null;
                PlusEnvironment.Game.ClientManager.SendPacket(new ModeratorSupportTicketComposer(Session.GetHabbo().Id, Ticket), "mod_tool");
            }
        }
    }
}