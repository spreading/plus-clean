﻿using System.Collections.Generic;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.Database.Interfaces;

namespace Plus.Communication.Packets.Incoming.Moderation
{
    class GetModeratorUserRoomVisitsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (!Session.GetHabbo().Permissions.HasRight("mod_tool"))
                return;

            int UserId = Packet.PopInt();
            GameClient Target = PlusEnvironment.Game.ClientManager.GetClientByUserID(UserId);
            if (Target == null)
                return;
            
            Dictionary<double, RoomData> Visits = new Dictionary<double, RoomData>();
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `room_id`, `entry_timestamp` FROM `user_roomvisits` WHERE `user_id` = @id ORDER BY `entry_timestamp` DESC LIMIT 50");
                dbClient.AddParameter("id", UserId);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        RoomData RData = PlusEnvironment.Game.RoomManager.GenerateRoomData(reader.GetInt32("room_id"));
                        if (RData == null)
                            return;

                        if (!Visits.ContainsKey(reader.GetDouble("entry_timestamp")))
                            Visits.Add(reader.GetDouble("entry_timestamp"), RData);
                    }
            }

            Session.SendPacket(new ModeratorUserRoomVisitsComposer(Target.GetHabbo(), Visits));
        }
    }
}