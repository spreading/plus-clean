﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Quests;

namespace Plus.Communication.Packets.Incoming.Messenger
{
    class RequestBuddyEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().Messenger == null)
                return;

            if (Session.GetHabbo().Messenger.RequestBuddy(Packet.PopString()))
                PlusEnvironment.Game.QuestManager.ProgressUserQuest(Session, QuestType.SOCIAL_FRIEND);
        }
    }
}
