﻿using Plus.Communication.Packets.Outgoing.Rooms.Avatar;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Quests;
using Plus.HabboHotel.Rooms;

namespace Plus.Communication.Packets.Incoming.Rooms.Avatar
{
    public class ActionEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (!Session.GetHabbo().InRoom)
                return;

            int Action = Packet.PopInt();

            if (!PlusEnvironment.Game.RoomManager.TryGetRoom(Session.GetHabbo().CurrentRoomId, out Room Room))
                return;

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            if (User.DanceId > 0)
                User.DanceId = 0;

            if (Session.GetHabbo().Effects.CurrentEffect > 0)
                Room.SendPacket(new AvatarEffectComposer(User.VirtualId, 0));

            User.UnIdle();
            Room.SendPacket(new ActionComposer(User.VirtualId, Action));

            if (Action == 5) // idle
            {
                User.IsAsleep = true;
                Room.SendPacket(new SleepComposer(User, true));
            }

            PlusEnvironment.Game.QuestManager.ProgressUserQuest(Session, QuestType.SOCIAL_WAVE);
        }
    }
}