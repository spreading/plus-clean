﻿using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Catalog.Marketplace;
using Plus.Communication.Packets.Outgoing.Marketplace;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Database.Interfaces;


namespace Plus.Communication.Packets.Incoming.Marketplace
{
    class BuyOfferEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            int OfferId = Packet.PopInt();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `state`,`timestamp`,`total_price`,`extra_data`,`item_id`,`furni_id`,`user_id`,`limited_number`,`limited_stack` FROM `catalog_marketplace_offers` WHERE `offer_id` = @OfferId LIMIT 1");
                dbClient.AddParameter("OfferId", OfferId);

                using (var dataReader = dbClient.ExecuteReader())
                    if (dataReader.Read())
                    {
                        if (dataReader.GetString("state") == "2")
                        {
                            Session.SendNotification("Oops, this offer is no longer available.");
                            this.ReloadOffers(Session);
                            return;
                        }

                        if (PlusEnvironment.Game.CatalogManager.GetMarketplace().FormatTimestamp() > (dataReader.GetDouble("timestamp")))
                        {
                            Session.SendNotification("Oops, this offer has expired..");
                            this.ReloadOffers(Session);
                            return;
                        }

                        if (!PlusEnvironment.Game.ItemManager.GetItem(dataReader.GetInt32("item_id"), out ItemData Item))
                        {
                            Session.SendNotification("Item isn't in the hotel anymore.");
                            this.ReloadOffers(Session);
                            return;
                        }
                        else
                        {
                            if (dataReader.GetInt32("user_id") == Session.GetHabbo().Id)
                            {
                                Session.SendNotification("To prevent average boosting you cannot purchase your own marketplace offers.");
                                return;
                            }

                            if (dataReader.GetInt32("total_price") > Session.GetHabbo().Credits)
                            {
                                Session.SendNotification("Oops, you do not have enough credits for this.");
                                return;
                            }

                            Session.GetHabbo().Credits -= dataReader.GetInt32("total_price");
                            Session.SendPacket(new CreditBalanceComposer(Session.GetHabbo().Credits));


                            Item GiveItem = ItemFactory.CreateSingleItem(Item, Session.GetHabbo(), dataReader.GetString("extra_data"), dataReader.GetString("extra_data"), dataReader.GetInt32("furni_id"), dataReader.GetInt32("limited_number"), dataReader.GetInt32("limited_stack"));
                            if (GiveItem != null)
                            {
                                Session.GetHabbo().InventoryComponent.TryAddItem(GiveItem);
                                Session.SendPacket(new FurniListNotificationComposer(GiveItem.Id, 1));

                                Session.SendPacket(new Plus.Communication.Packets.Outgoing.Catalog.PurchaseOKComposer());
                                Session.SendPacket(new FurniListAddComposer(GiveItem));
                                Session.SendPacket(new FurniListUpdateComposer());
                            }


                            using (IQueryAdapter newDbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                            {
                                newDbClient.RunQuery("UPDATE `catalog_marketplace_offers` SET `state` = '2' WHERE `offer_id` = '" + OfferId + "' LIMIT 1");

                                int Id = 0;
                                newDbClient.SetQuery("SELECT `id` FROM `catalog_marketplace_data` WHERE `sprite` = " + Item.SpriteId + " LIMIT 1;");
                                Id = newDbClient.GetInteger();

                                if (Id > 0)
                                    newDbClient.RunQuery("UPDATE `catalog_marketplace_data` SET `sold` = `sold` + 1, `avgprice` = (avgprice + " + dataReader.GetInt32("total_price") + ") WHERE `id` = " + Id + " LIMIT 1;");
                                else
                                    newDbClient.RunQuery("INSERT INTO `catalog_marketplace_data` (`sprite`, `sold`, `avgprice`) VALUES ('" + Item.SpriteId + "', '1', '" + dataReader.GetInt32("total_price") + "')");


                                if (PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketAverages.ContainsKey(Item.SpriteId) && PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketCounts.ContainsKey(Item.SpriteId))
                                {
                                    int num3 = PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketCounts[Item.SpriteId];
                                    int num4 = (PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketAverages[Item.SpriteId] += dataReader.GetInt32("total_price"));

                                    PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketAverages.Remove(Item.SpriteId);
                                    PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketAverages.Add(Item.SpriteId, num4);
                                    PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketCounts.Remove(Item.SpriteId);
                                    PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketCounts.Add(Item.SpriteId, num3 + 1);
                                }
                                else
                                {
                                    if (!PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketAverages.ContainsKey(Item.SpriteId))
                                        PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketAverages.Add(Item.SpriteId, dataReader.GetInt32("total_price"));

                                    if (!PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketCounts.ContainsKey(Item.SpriteId))
                                        PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketCounts.Add(Item.SpriteId, 1);
                                }
                            }
                        }
                    }
                    else
                    {
                        this.ReloadOffers(Session);
                        return;
                    }

            }
            this.ReloadOffers(Session);
        }


        private void ReloadOffers(GameClient Session)
        {
            int MinCost = -1;
            int MaxCost = -1;
            string SearchQuery = "";
            int FilterMode = 1;

            StringBuilder builder = new StringBuilder();
            string str = "";
            builder.Append("WHERE `state` = '1' AND `timestamp` >= " + PlusEnvironment.Game.CatalogManager.GetMarketplace().FormatTimestampString());
            if (MinCost >= 0)
            {
                builder.Append(" AND `total_price` > " + MinCost);
            }
            if (MaxCost >= 0)
            {
                builder.Append(" AND `total_price` < " + MaxCost);
            }
            switch (FilterMode)
            {
                case 1:
                    str = "ORDER BY `asking_price` DESC";
                    break;

                default:
                    str = "ORDER BY `asking_price` ASC";
                    break;
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {

                dbClient.SetQuery("SELECT `offer_id`,`item_type`,`sprite_id`,`total_price`,`limited_number`,`limited_stack` FROM `catalog_marketplace_offers` " + builder.ToString() + " " + str + " LIMIT 500");
                dbClient.AddParameter("search_query", "%" + SearchQuery + "%");
                if (SearchQuery.Length >= 1)
                {
                    builder.Append(" AND `public_name` LIKE @search_query");
                }

                PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItems.Clear();
                PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItemKeys.Clear();
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        if (!PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItemKeys.Contains(reader.GetInt32("offer_id")))
                        {
                            MarketOffer item = new MarketOffer(reader.GetInt32("offer_id"), reader.GetInt32("sprite_id"), reader.GetInt32("total_price"), int.Parse(reader.GetString("item_type")), reader.GetInt32("limited_number"), reader.GetInt32("limited_stack"));
                            PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItemKeys.Add(reader.GetInt32("offer_id"));
                            PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItems.Add(item);
                        }
            }


            Dictionary<int, MarketOffer> dictionary = new Dictionary<int, MarketOffer>();
            Dictionary<int, int> dictionary2 = new Dictionary<int, int>();

            foreach (MarketOffer item in PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItems)
            {
                if (dictionary.ContainsKey(item.SpriteId))
                {
                    if (dictionary[item.SpriteId].TotalPrice > item.TotalPrice)
                    {
                        dictionary.Remove(item.SpriteId);
                        dictionary.Add(item.SpriteId, item);
                    }

                    int num = dictionary2[item.SpriteId];
                    dictionary2.Remove(item.SpriteId);
                    dictionary2.Add(item.SpriteId, num + 1);
                }
                else
                {
                    dictionary.Add(item.SpriteId, item);
                    dictionary2.Add(item.SpriteId, 1);
                }
            }

            Session.SendPacket(new MarketPlaceOffersComposer(MinCost, MaxCost, dictionary, dictionary2));
        }
    }
}