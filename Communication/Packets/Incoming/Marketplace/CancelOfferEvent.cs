﻿using Plus.HabboHotel.Items;
using Plus.Communication.Packets.Outgoing.Marketplace;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Database.Interfaces;


namespace Plus.Communication.Packets.Incoming.Marketplace
{
    class CancelOfferEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            Item giveItem = null;
            int OfferId = Packet.PopInt();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `furni_id`, `item_id`, `user_id`, `extra_data`, `offer_id`, `state`, `timestamp`, `limited_number`, `limited_stack` FROM `catalog_marketplace_offers` WHERE `offer_id` = @OfferId LIMIT 1");
                dbClient.AddParameter("OfferId", OfferId);

                using (var reader = dbClient.ExecuteReader())
                    if (reader.HasRows)
                        if (reader.Read())
                        {
                            if (reader.GetInt32("user_id") != Session.GetHabbo().Id)
                            {
                                Session.SendPacket(new MarketplaceCancelOfferResultComposer(OfferId, false));
                                return;
                            }

                            if (!PlusEnvironment.Game.ItemManager.GetItem(reader.GetInt32("item_id"), out ItemData Item))
                            {
                                Session.SendPacket(new MarketplaceCancelOfferResultComposer(OfferId, false));
                                return;
                            }

                            giveItem = ItemFactory.CreateSingleItem(Item, Session.GetHabbo(), reader.GetString("extra_data"), reader.GetString("extra_data"), reader.GetInt32("furni_id"), reader.GetInt32("limited_number"), reader.GetInt32("limited_stack"));
                        }
                        else
                        {
                            Session.SendPacket(new MarketplaceCancelOfferResultComposer(OfferId, false));
                            return;
                        }
            }

            Session.SendPacket(new FurniListNotificationComposer(giveItem.Id, 1));
            Session.SendPacket(new FurniListUpdateComposer());

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("DELETE FROM `catalog_marketplace_offers` WHERE `offer_id` = @OfferId AND `user_id` = @UserId LIMIT 1");
                dbClient.AddParameter("OfferId", OfferId);
                dbClient.AddParameter("UserId", Session.GetHabbo().Id);
                dbClient.RunQuery();
            }

            Session.GetHabbo().InventoryComponent.UpdateItems(true);
            Session.SendPacket(new MarketplaceCancelOfferResultComposer(OfferId, true));
        }
    }
}
