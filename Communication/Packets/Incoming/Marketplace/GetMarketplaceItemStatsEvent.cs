﻿using Plus.Communication.Packets.Outgoing.Marketplace;
using Plus.Database.Interfaces;

namespace Plus.Communication.Packets.Incoming.Marketplace
{
    class GetMarketplaceItemStatsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int ItemId = Packet.PopInt();
            int SpriteId = Packet.PopInt();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `avgprice` FROM `catalog_marketplace_data` WHERE `sprite` = @SpriteId LIMIT 1");
                dbClient.AddParameter("SpriteId", SpriteId);
                using (var reader = dbClient.ExecuteReader())
                    if (reader.Read())
                        Session.SendPacket(new MarketplaceItemStatsComposer(ItemId, SpriteId, reader.GetInt32("avgprice")));
                    else
                        Session.SendPacket(new MarketplaceItemStatsComposer(ItemId, SpriteId, 0));
            }
        }
    }
}