﻿using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Catalog.Marketplace;
using Plus.Communication.Packets.Outgoing.Marketplace;
using Plus.Database.Interfaces;


namespace Plus.Communication.Packets.Incoming.Marketplace
{
    class GetOffersEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int MinCost = Packet.PopInt();
            int MaxCost = Packet.PopInt();
            string SearchQuery = Packet.PopString();
            int FilterMode = Packet.PopInt();

            
            StringBuilder builder = new StringBuilder();
            string str = "";
            builder.Append("WHERE `state` = '1' AND `timestamp` >= " + PlusEnvironment.Game.CatalogManager.GetMarketplace().FormatTimestampString());
            if (MinCost >= 0)
                builder.Append(" AND `total_price` > " + MinCost);


            if (MaxCost >= 0)
                builder.Append(" AND `total_price` < " + MaxCost);

            switch (FilterMode)
            {
                case 1:
                    str = "ORDER BY `asking_price` DESC";
                    break;

                default:
                    str = "ORDER BY `asking_price` ASC";
                    break;
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {

                dbClient.SetQuery("SELECT `offer_id`, `item_type`, `sprite_id`, `total_price`, `limited_number`,`limited_stack` FROM `catalog_marketplace_offers` " + builder.ToString() + " " + str + " LIMIT 500");
                dbClient.AddParameter("search_query", "%" + SearchQuery + "%");
                if (SearchQuery.Length >= 1)
                {
                    builder.Append(" AND `public_name` LIKE @search_query");
                }

                PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItems.Clear();
                PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItemKeys.Clear();
                using (var reader = dbClient.ExecuteReader())
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                            if (!PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItemKeys.Contains(reader.GetInt32("offer_id")))
                            {
                                PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItemKeys.Add(reader.GetInt32("offer_id"));
                                PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItems.Add(new MarketOffer(reader.GetInt32("offer_id"), reader.GetInt32("sprite_id"), reader.GetInt32("total_price"), int.Parse(reader.GetString("item_type")), reader.GetInt32("limited_number"), reader.GetInt32("limited_stack")));
                            }
                    }
            }

            Dictionary<int, MarketOffer> dictionary = new Dictionary<int, MarketOffer>();
            Dictionary<int, int> dictionary2 = new Dictionary<int, int>();

            foreach (MarketOffer item in PlusEnvironment.Game.CatalogManager.GetMarketplace().MarketItems)
            {
                if (dictionary.ContainsKey(item.SpriteId))
                {
                    if (item.LimitedNumber > 0)
                    {
                        if (!dictionary.ContainsKey(item.OfferID))
                            dictionary.Add(item.OfferID, item);
                        if (!dictionary2.ContainsKey(item.OfferID))
                            dictionary2.Add(item.OfferID, 1);
                    }
                    else
                    {
                        if (dictionary[item.SpriteId].TotalPrice > item.TotalPrice)
                        {
                            dictionary.Remove(item.SpriteId);
                            dictionary.Add(item.SpriteId, item);
                        }

                        int num = dictionary2[item.SpriteId];
                        dictionary2.Remove(item.SpriteId);
                        dictionary2.Add(item.SpriteId, num + 1);
                    }
                }
                else
                {
                    if (!dictionary.ContainsKey(item.SpriteId))
                        dictionary.Add(item.SpriteId, item);
                    if (!dictionary2.ContainsKey(item.SpriteId))
                        dictionary2.Add(item.SpriteId, 1);
                }
            }

            Session.SendPacket(new MarketPlaceOffersComposer(MinCost, MaxCost, dictionary, dictionary2));
        }
    }
}
