﻿using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Database.Interfaces;

namespace Plus.Communication.Packets.Incoming.Marketplace
{
    class RedeemOfferCreditsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int CreditsOwed = 0;
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `asking_price` FROM `catalog_marketplace_offers` WHERE `user_id` = '" + Session.GetHabbo().Id + "' AND `state` = '2'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        CreditsOwed += reader.GetInt32("asking_price");

                        if (CreditsOwed >= 1)
                        {
                            Session.GetHabbo().Credits += CreditsOwed;
                            Session.SendPacket(new CreditBalanceComposer(Session.GetHabbo().Credits));
                        }

                        dbClient.RunQuery("DELETE FROM `catalog_marketplace_offers` WHERE `user_id` = '" + Session.GetHabbo().Id + "' AND `state` = '2'");
                    }
            }
        }
    }
}