﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Quests;
using Plus.Communication.Packets.Outgoing.Rooms.Avatar;
using Plus.Communication.Packets.Outgoing.Users;

namespace Plus.Communication.Packets.Incoming.Users
{
    class RespectUserEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            if (!Session.GetHabbo().InRoom || Session.GetHabbo().Stats.DailyRespectPoints <= 0)
                return;

            if (!PlusEnvironment.Game.RoomManager.TryGetRoom(Session.GetHabbo().CurrentRoomId, out Room Room))
                return;

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Packet.PopInt());
            if (User == null || User.GetClient() == null || User.GetClient().GetHabbo().Id == Session.GetHabbo().Id || User.IsBot)
                return;

            RoomUser ThisUser = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (ThisUser == null)
                return;
           
            PlusEnvironment.Game.QuestManager.ProgressUserQuest(Session, QuestType.SOCIAL_RESPECT);
            PlusEnvironment.Game.AchievementManager.ProgressAchievement(Session, "ACH_RespectGiven", 1);
            PlusEnvironment.Game.AchievementManager.ProgressAchievement(User.GetClient(), "ACH_RespectEarned", 1);

            Session.GetHabbo().Stats.DailyRespectPoints -= 1;
            Session.GetHabbo().Stats.RespectGiven += 1;
            User.GetClient().GetHabbo().Stats.Respect += 1;

            if (Room.RespectNotificationsEnabled)
                Room.SendPacket(new RespectNotificationComposer(User.GetClient().GetHabbo().Id, User.GetClient().GetHabbo().Stats.Respect));
            Room.SendPacket(new ActionComposer(ThisUser.VirtualId, 7));
        }
    }
}