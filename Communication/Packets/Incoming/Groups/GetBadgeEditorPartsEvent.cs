﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.Groups;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class GetBadgeEditorPartsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient session, ClientPacket packet)
        {
            session.SendPacket(new BadgeEditorPartsComposer(
                PlusEnvironment.Game.GroupManager.BadgeBases,
                PlusEnvironment.Game.GroupManager.BadgeSymbols,
                PlusEnvironment.Game.GroupManager.BadgeBaseColours,
                PlusEnvironment.Game.GroupManager.BadgeSymbolColours,
                PlusEnvironment.Game.GroupManager.BadgeBackColours));
        }
    }
}