﻿using Plus.HabboHotel.Quests;
using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing.Quests;

namespace Plus.Communication.Packets.Incoming.Quests
{
    class GetCurrentQuestEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (!Session.GetHabbo().InRoom)
                return;

            Quest UserQuest = PlusEnvironment.Game.QuestManager.GetQuest(Session.GetHabbo().QuestLastCompleted);
            Quest NextQuest = PlusEnvironment.Game.QuestManager.GetNextQuestInSeries(UserQuest.Category, UserQuest.Number + 1);

            if (NextQuest == null)
                return;

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.RunQuery("REPLACE INTO `user_quests`(`user_id`,`quest_id`) VALUES (" + Session.GetHabbo().Id + ", " + NextQuest.Id + ")");
                dbClient.RunQuery("UPDATE `user_stats` SET `quest_id` = '" + NextQuest.Id + "' WHERE `id` = '" + Session.GetHabbo().Id + "' LIMIT 1");
            }

            Session.GetHabbo().Stats.QuestID = NextQuest.Id;
            PlusEnvironment.Game.QuestManager.GetList(Session, null);
            Session.SendPacket(new QuestStartedComposer(Session, NextQuest));
        }
    }
}
