﻿using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;

namespace Plus.Communication.RCON.Commands.User
{
    class ReloadUserRankCommand : IRCONCommand
    {
        public string Description
        {
            get { return "This command is used to reload a users rank and permissions."; }
        }

        public string Parameters
        {
            get { return "%userId%"; }
        }

        public bool TryExecute(string[] parameters)
        {
            int userId = 0;
            if (!int.TryParse(parameters[0].ToString(), out userId))
                return false;

            GameClient client = PlusEnvironment.Game.ClientManager.GetClientByUserID(userId);
            if (client == null || client.GetHabbo() == null)
                return false;

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `rank` FROM `users` WHERE `id` = @userId LIMIT 1");
                dbClient.AddParameter("userId", userId);
                client.GetHabbo().Rank = dbClient.GetInteger();
            }

            client.GetHabbo().Permissions.Init(client.GetHabbo());

            if (client.GetHabbo().Permissions.HasRight("mod_tickets"))
            {
                client.SendPacket(new ModeratorInitComposer(
                  PlusEnvironment.Game.ModerationManager.UserMessagePresets,
                  PlusEnvironment.Game.ModerationManager.RoomMessagePresets,
                  PlusEnvironment.Game.ModerationManager.GetTickets));
            }
            return true;
        }
    }
}