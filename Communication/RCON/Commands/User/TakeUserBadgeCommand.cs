﻿using System;

using Plus.HabboHotel.GameClients;

namespace Plus.Communication.RCON.Commands.User
{
    class TakeUserBadgeCommand : IRCONCommand
    {
        public string Description
        {
            get { return "This command is used to take a badge from a user."; }
        }

        public string Parameters
        {
            get { return "%userId% %badgeId%"; }
        }

        public bool TryExecute(string[] parameters)
        {
            int userId = 0;
            if (!int.TryParse(parameters[0].ToString(), out userId))
                return false;

            GameClient client = PlusEnvironment.Game.ClientManager.GetClientByUserID(userId);
            if (client == null || client.GetHabbo() == null)
                return false;

            // Validate the badge
            if (string.IsNullOrEmpty(Convert.ToString(parameters[1])))
                return false;

            string badge = Convert.ToString(parameters[1]);

            if (client.GetHabbo().BadgeComponent.HasBadge(badge))
            {
                client.GetHabbo().BadgeComponent.RemoveBadge(badge);
            }
            return true;
        }
    }
}