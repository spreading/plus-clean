﻿using System.Linq;
using Plus.HabboHotel.GameClients;

namespace Plus.Communication.RCON.Commands.Hotel
{
    class ReloadRanksCommand : IRCONCommand
    {
        public string Description
        {
            get { return "This command is used to reload user permissions."; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public bool TryExecute(string[] parameters)
        {
            PlusEnvironment.Game.PermissionManager.Init();

            foreach (GameClient client in PlusEnvironment.Game.ClientManager.GetClients.ToList())
            {
                if (client == null || client.GetHabbo() == null || client.GetHabbo().Permissions == null)
                    continue;

                client.GetHabbo().Permissions.Init(client.GetHabbo());
            }
            
            return true;
        }
    }
}