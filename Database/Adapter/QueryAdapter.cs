﻿using System;
using MySql.Data.MySqlClient;
using Plus.Database.Interfaces;
using Plus.Core;

namespace Plus.Database.Adapter
{
    public class QueryAdapter : IRegularQueryAdapter
    {
        protected IDatabaseClient client;
        protected MySqlCommand command;

        public bool dbEnabled = true;

        public QueryAdapter(IDatabaseClient Client)
        {
            client = Client;
        }

        public void AddParameter(string parameterName, object val)
        {
            command.Parameters.AddWithValue("@" + parameterName, val);
        }

        public bool FindsResult()
        {
            bool hasRows = false;
            try
            {
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    hasRows = reader.HasRows;
                }
            }
            catch (Exception exception)
            {
                ExceptionLogger.LogQueryError(command.CommandText, exception);
            }

            return hasRows;
        }

        public string GetCommand() =>
            command.CommandText;

        public MySqlDataReader ExecuteReader()
        {
            MySqlDataReader reader = null;
            try
            {
                reader = command.ExecuteReader();
                return reader;
            }
            catch (MySqlException ex)
            {
                ExceptionLogger.LogQueryError("MySQL Error: ", ex);

                return null;
            }
            finally
            {
                command.CommandText = string.Empty;
                command.Parameters.Clear();
            }
        }

        public int GetInteger()
        {
            int result = 0;
            try
            {
                object obj2 = command.ExecuteScalar();
                if (obj2 != null)
                {
                    int.TryParse(obj2.ToString(), out result);
                }
            }
            catch (Exception exception)
            {
                ExceptionLogger.LogQueryError(command.CommandText, exception);
            }

            return result;
        }

        public string GetString()
        {
            string str = string.Empty;
            try
            {
                object obj2 = command.ExecuteScalar();
                if (obj2 != null)
                {
                    str = obj2.ToString();
                }
            }
            catch (Exception exception)
            {
                ExceptionLogger.LogQueryError(command.CommandText, exception);
            }

            return str;
        }

        public void RunQuery(string query)
        {
            if (!dbEnabled)
                return;

            SetQuery(query);
            RunQuery();
        }

        public void SetQuery(string query)
        {
            command.Parameters.Clear();
            command.CommandText = query;
        }

        public long InsertQuery()
        {
            if (!dbEnabled)
                return 0;

            long lastInsertedId = 0L;
            try
            {
                command.ExecuteScalar();
                lastInsertedId = command.LastInsertedId;
            }
            catch (Exception exception)
            {
                ExceptionLogger.LogQueryError(command.CommandText, exception);
            }
            return lastInsertedId;
        }

        public void RunQuery()
        {
            if (!dbEnabled)
                return;

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                ExceptionLogger.LogQueryError(command.CommandText, exception);
            }
        }
    }
}