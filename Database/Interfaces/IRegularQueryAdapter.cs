﻿using MySql.Data.MySqlClient;

namespace Plus.Database.Interfaces
{
    public interface IRegularQueryAdapter
    {
        void AddParameter(string name, object query);
        bool FindsResult();
        int GetInteger();
        string GetString();
        string GetCommand();
        MySqlDataReader ExecuteReader();
        void RunQuery(string query);
        void SetQuery(string query);
    }
}