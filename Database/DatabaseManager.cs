﻿using System;
using MySql.Data.MySqlClient;
using Plus.Database.Interfaces;

namespace Plus.Database
{
    public sealed class DatabaseManager
    {
        private readonly string _connectionStr;

        public DatabaseManager(string conString)
        {
            _connectionStr = conString;
        }

        public bool IsConnected()
        {
            try
            {
                MySqlConnection Con = new MySqlConnection(this._connectionStr);
                Con.Open();
                MySqlCommand CMD = Con.CreateCommand();
                CMD.CommandText = "SELECT 1+1";
                CMD.ExecuteNonQuery();
                CMD.Dispose();
                Con.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
                return false;
            }

            return true;
        }

        public IQueryAdapter GetQueryReactor()
        {
            IDatabaseClient client = new DatabaseConnection(_connectionStr);
            client.Connect();
            return client.GetQueryReactor();
        }
    }
}