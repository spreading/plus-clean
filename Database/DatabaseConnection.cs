﻿using System;
using System.Data;

using MySql.Data.MySqlClient;
using Plus.Database.Interfaces;
using Plus.Database.Adapter;

namespace Plus.Database
{
    public class DatabaseConnection : IDatabaseClient, IDisposable
    {
        private readonly IQueryAdapter _adapter;
        private readonly MySqlConnection _con;

        public DatabaseConnection(string ConnectionStr)
        {
            this._con = new MySqlConnection(ConnectionStr);
            this._adapter = new NormalQueryReactor(this);
        }

        public void Connect()
        {
            if (_con.State == ConnectionState.Closed)
            {
                try
                {
                    _con.Open();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }

        public void Disconnect()
        {
            if (_con.State == ConnectionState.Open)
            {
                _con.Close();
            }
        }

        public IQueryAdapter GetQueryReactor()
        {
            return this._adapter;
        }

        public void ReportDone()
        {
            Dispose();
        }

        public MySqlCommand CreateNewCommand()
        {
            return _con.CreateCommand();
        }

        public void Dispose()
        {
            if (this._con.State == ConnectionState.Open)
            {
                this._con.Close();
            }

            this._con.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}