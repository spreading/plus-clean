﻿using Plus.Utilities;

namespace Plus.HabboHotel.Rewards
{
    public class Reward
    {
        public double RewardStart { get; }
        public double RewardEnd { get; }
        public RewardType Type { get; }
        public string RewardData { get; }
        public string Message { get; }
        public Reward(double Start, double End, string Type, string RewardData, string Message)
        {
            this.RewardStart = Start;
            this.RewardEnd = End;
            this.Type = RewardTypeUtility.GetType(Type);
            this.RewardData = RewardData;
            this.Message = Message;
        }

        public bool isActive()
        {
            double Now = UnixTimestamp.GetNow();
            return (Now >= RewardStart && Now <= RewardEnd);
        }
    }
}
