﻿using Plus.Database.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;

namespace Plus.HabboHotel.Rewards
{
    public class RewardManager
    {
        private ConcurrentDictionary<int, Reward> _rewards;
        private ConcurrentDictionary<int, List<int>> _rewardLogs;

        public RewardManager()
        {
            this._rewards = new ConcurrentDictionary<int, Reward>();
            this._rewardLogs = new ConcurrentDictionary<int, List<int>>();

            this.Reload();   
        }

        public void Reload()
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `server_rewards` WHERE enabled = '1'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _rewards.TryAdd(reader.GetInt32("id"), new Reward(reader.GetDouble("reward_start"), reader.GetDouble("reward_end"), reader.GetString("reward_type"), reader.GetString("reward_data"), reader.GetString("message")));

                dbClient.SetQuery("SELECT * FROM `server_reward_logs`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int Id = reader.GetInt32("user_id");
                        int RewardId = reader.GetInt32("reward_id");

                        if (!_rewardLogs.ContainsKey(Id))
                            _rewardLogs.TryAdd(Id, new List<int>());

                        if (!_rewardLogs[Id].Contains(RewardId))
                            _rewardLogs[Id].Add(RewardId);
                    }
            }
        }

        public bool HasReward(int Id, int RewardId)
        {
            if (!_rewardLogs.ContainsKey(Id))
                return false;

            if (_rewardLogs[Id].Contains(RewardId))
                return true;

            return false;
        }

        public void LogReward(int Id, int RewardId)
        {
            if (!_rewardLogs.ContainsKey(Id))
                _rewardLogs.TryAdd(Id, new List<int>());

            if (!_rewardLogs[Id].Contains(RewardId))
                _rewardLogs[Id].Add(RewardId);

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("INSERT INTO `server_reward_logs` VALUES ('', @userId, @rewardId)");
                dbClient.AddParameter("userId", Id);
                dbClient.AddParameter("rewardId", RewardId);
                dbClient.RunQuery();
            }
        }

        public void CheckRewards(GameClient Session)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            foreach (KeyValuePair<int, Reward> Entry in _rewards)
            {
                int Id = Entry.Key;
                Reward Reward = Entry.Value;

                if (this.HasReward(Session.GetHabbo().Id, Id))
                    continue;

                if (Reward.isActive())
                {
                    switch (Reward.Type)
                    {
                        case RewardType.BADGE:
                            {
                                if (!Session.GetHabbo().BadgeComponent.HasBadge(Reward.RewardData))
                                    Session.GetHabbo().BadgeComponent.GiveBadge(Reward.RewardData, true, Session);
                                break;
                            }

                        case RewardType.CREDITS:
                            {
                                Session.GetHabbo().Credits += Convert.ToInt32(Reward.RewardData);
                                Session.SendPacket(new CreditBalanceComposer(Session.GetHabbo().Credits));
                                break;
                            }

                        case RewardType.DUCKETS:
                            {
                                Session.GetHabbo().Duckets += Convert.ToInt32(Reward.RewardData);
                                Session.SendPacket(new HabboActivityPointNotificationComposer(Session.GetHabbo().Duckets, Convert.ToInt32(Reward.RewardData)));
                                break;
                            }

                        case RewardType.DIAMONDS:
                            {
                                Session.GetHabbo().Diamonds += Convert.ToInt32(Reward.RewardData);
                                Session.SendPacket(new HabboActivityPointNotificationComposer(Session.GetHabbo().Diamonds, Convert.ToInt32(Reward.RewardData), 5));
                                break;
                            }
                    }

                    if (!String.IsNullOrEmpty(Reward.Message))
                        Session.SendNotification(Reward.Message);

                    this.LogReward(Session.GetHabbo().Id, Id);
                }
                else
                    continue;
            }
        }
    }
}