﻿using System.Collections.Generic;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Catalog.Vouchers
{
    public class VoucherManager
    {
        private readonly Dictionary<string, Voucher> _vouchers;

        public VoucherManager()
        {
            this._vouchers = new Dictionary<string, Voucher>();
        }

        public void Init()
        {
            if (this._vouchers.Count > 0)
                this._vouchers.Clear();
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `voucher`,`type`,`value`,`current_uses`,`max_uses` FROM `catalog_vouchers` WHERE `enabled` = '1'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        _vouchers.Add(reader.GetString("voucher"), new Voucher(reader.GetString("voucher"), reader.GetString("type"), reader.GetInt32("value"), reader.GetInt32("current_uses"), reader.GetInt32("max_uses")));
                    }
            }
        }

        public bool TryGetVoucher(string Code, out Voucher Voucher)
        {
            if (this._vouchers.TryGetValue(Code, out Voucher))
                return true;
            return false;
        }
    }
}