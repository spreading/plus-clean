﻿using System.Collections.Generic;
using Plus.HabboHotel.Items;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Catalog
{
    public class CatalogDeal
    {
        public int Id { get; }
        public List<CatalogItem> ItemDataList { get; private set; }
        public string DisplayName { get; }
        public int RoomId { get; }

        public CatalogDeal(int Id, string Items, string DisplayName, int RoomId, ItemDataManager ItemDataManager)
        {
            this.Id = Id;
            this.DisplayName = DisplayName;
            this.RoomId = RoomId;
            this.ItemDataList = new List<CatalogItem>();

            if (RoomId != 0)
            {
                Dictionary<int, int> roomItems = new Dictionary<int, int>();

                using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                {
                    dbClient.SetQuery("SELECT `items`.base_item, COALESCE(`items_groups`.`group_id`, 0) AS `group_id` FROM `items` LEFT OUTER JOIN `items_groups` ON `items`.`id` = `items_groups`.`id` WHERE `items`.`room_id` = @rid;");
                    dbClient.AddParameter("rid", RoomId);
                    using (var reader = dbClient.ExecuteReader())
                        while (reader.Read())
                        {
                            int item_id = reader.GetInt32("base_item");
                            if (roomItems.ContainsKey(item_id))
                                roomItems[item_id]++;
                            else
                                roomItems.Add(item_id, 1);
                        }
                }

                foreach (var roomItem in roomItems)
                    Items += roomItem.Key + "*" + roomItem.Value + ";";

                if (roomItems.Count > 0)
                    Items = Items.Remove(Items.Length - 1);
            }

            string[] SplitItems = Items.Split(';');
            foreach (string Split in SplitItems)
            {
                string[] Item = Split.Split('*');
                if (!int.TryParse(Item[0], out int ItemId) || !int.TryParse(Item[1], out int Amount))
                    continue;

                if (!ItemDataManager.GetItem(ItemId, out ItemData Data))
                    continue;

                ItemDataList.Add(new CatalogItem(0, ItemId, Data, string.Empty, 0, 0, 0, 0, Amount, 0, 0, false, "", "", 0));
            }
        }
    }
}