﻿using System.Collections.Generic;
using log4net;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Catalog.Pets;
using Plus.HabboHotel.Catalog.Vouchers;
using Plus.HabboHotel.Catalog.Marketplace;
using Plus.HabboHotel.Catalog.Clothing;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Catalog
{
    public class CatalogManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CatalogManager));

        private MarketplaceManager _marketplace;
        private PetRaceManager _petRaceManager;
        private VoucherManager _voucherManager;
        private ClothingManager _clothingManager;

        private Dictionary<int, int> _itemOffers;
        private readonly Dictionary<int, CatalogPage> _pages;
        private readonly Dictionary<int, CatalogBot> _botPresets;
        private readonly Dictionary<int, Dictionary<int, CatalogItem>> _items;
        private readonly Dictionary<int, CatalogDeal> _deals;
        private readonly Dictionary<int, CatalogPromotion> _promotions;

        public CatalogManager()
        {
            this._marketplace = new MarketplaceManager();
            this._petRaceManager = new PetRaceManager();

            this._voucherManager = new VoucherManager();
            this._voucherManager.Init();

            this._clothingManager = new ClothingManager();
            this._clothingManager.Init();

            this._itemOffers = new Dictionary<int, int>();
            this._pages = new Dictionary<int, CatalogPage>();
            this._botPresets = new Dictionary<int, CatalogBot>();
            this._items = new Dictionary<int, Dictionary<int, CatalogItem>>();
            this._deals = new Dictionary<int, CatalogDeal>();
            this._promotions = new Dictionary<int, CatalogPromotion>();
        }

        public void Init(ItemDataManager ItemDataManager)
        {
            if (this._pages.Count > 0)
                this._pages.Clear();
            if (this._botPresets.Count > 0)
                this._botPresets.Clear();
            if (this._items.Count > 0)
                this._items.Clear();
            if (this._deals.Count > 0)
                this._deals.Clear();
            if (this._promotions.Count > 0)
                this._promotions.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`item_id`,`catalog_name`,`cost_credits`,`cost_pixels`,`cost_diamonds`,`amount`,`page_id`,`limited_sells`,`limited_stack`,`offer_active`,`extradata`,`badge`,`offer_id` FROM `catalog_items`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        if (reader.GetInt32("amount") <= 0)
                            continue;

                        int ItemId = reader.GetInt32("id");
                        int PageId = reader.GetInt32("page_id");
                        int BaseId = reader.GetInt32("item_id");
                        int OfferId = reader.GetInt32("offer_id");

                        if (!ItemDataManager.GetItem(BaseId, out ItemData Data))
                        {
                            log.Error("Couldn't load Catalog Item " + ItemId + ", no furniture record found.");
                            continue;
                        }

                        if (!this._items.ContainsKey(PageId))
                            this._items[PageId] = new Dictionary<int, CatalogItem>();

                        if (OfferId != -1 && !this._itemOffers.ContainsKey(OfferId))
                            this._itemOffers.Add(OfferId, PageId);

                        this._items[PageId].Add(reader.GetInt32("id"), new CatalogItem(reader.GetInt32("id"), reader.GetInt32("item_id"),
                            Data, reader.GetString("catalog_name"), reader.GetInt32("page_id"), reader.GetInt32("cost_credits"), reader.GetInt32("cost_pixels"), reader.GetInt32("cost_diamonds"),
                            reader.GetInt32("amount"), reader.GetInt32("limited_sells"), reader.GetInt32("limited_stack"), PlusEnvironment.EnumToBool(reader.GetString("offer_active")),
                            reader.GetString("extradata"), reader.GetString("badge"), reader.GetInt32("offer_id")));
                    }

                dbClient.SetQuery("SELECT `id`, `items`, `name`, `room_id` FROM `catalog_deals`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int Id = reader.GetInt32("id");
                        string Items = reader.GetString("items");
                        string Name = reader.GetString("name");
                        int RoomId = reader.GetInt32("room_id");

                        CatalogDeal Deal = new CatalogDeal(Id, Items, Name, RoomId, ItemDataManager);

                        if (!this._deals.ContainsKey(Id))
                            this._deals.Add(Deal.Id, Deal);
                    }


                dbClient.SetQuery("SELECT `id`,`parent_id`,`caption`,`page_link`,`visible`,`enabled`,`min_rank`,`min_vip`,`icon_image`,`page_layout`,`page_strings_1`,`page_strings_2` FROM `catalog_pages` ORDER BY `order_num`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        this._pages.Add(reader.GetInt32("id"), new CatalogPage(reader.GetInt32("id"), reader.GetInt32("parent_id"), reader.GetString("enabled"), reader.GetString("caption"),
                            reader.GetString("page_link"), reader.GetInt32("icon_image"), reader.GetInt32("min_rank"), reader.GetInt32("min_vip"), reader.GetString("visible"), reader.GetString("page_layout"),
                            reader.GetString("page_strings_1"), reader.GetString("page_strings_2"),
                            this._items.ContainsKey(reader.GetInt32("id")) ? this._items[reader.GetInt32("id")] : new Dictionary<int, CatalogItem>(), ref this._itemOffers));

                dbClient.SetQuery("SELECT `id`,`name`,`figure`,`motto`,`gender`,`ai_type` FROM `catalog_bot_presets`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _botPresets.Add(reader.GetInt32("id"), new CatalogBot(reader.GetInt32("id"), reader.GetString("name"), reader.GetString("figure"), reader.GetString("motto"), reader.GetString("gender"), reader.GetString("ai_type")));

                dbClient.SetQuery("SELECT * FROM `catalog_promotions`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        if (!this._promotions.ContainsKey(reader.GetInt32("id")))
                            this._promotions.Add(reader.GetInt32("id"), new CatalogPromotion(reader.GetInt32("id"), reader.GetString("title"), reader.GetString("image"), reader.GetInt32("unknown"), reader.GetString("page_link"), reader.GetInt32("parent_id")));

                this._petRaceManager.Init();
                this._clothingManager.Init();
            }

            log.Info("Catalog Manager -> LOADED");
        }

        public bool TryGetBot(int ItemId, out CatalogBot Bot)
        {
            return this._botPresets.TryGetValue(ItemId, out Bot);
        }

        public Dictionary<int, int> ItemOffers {
            get { return this._itemOffers; }
        }

        public bool TryGetPage(int pageId, out CatalogPage page)
        {
            return this._pages.TryGetValue(pageId, out page);
        }

        public bool TryGetDeal(int dealId, out CatalogDeal deal)
        {
            return this._deals.TryGetValue(dealId, out deal);
        }

        public ICollection<CatalogPage> GetPages()
        {
            return this._pages.Values;
        }

        public ICollection<CatalogPromotion> GetPromotions()
        {
            return this._promotions.Values;
        }

        public MarketplaceManager GetMarketplace()
        {
            return this._marketplace;
        }

        public PetRaceManager GetPetRaceManager()
        {
            return this._petRaceManager;
        }

        public VoucherManager GetVoucherManager()
        {
            return this._voucherManager;
        }

        public ClothingManager GetClothingManager()
        {
            return this._clothingManager;
        }
    }
}