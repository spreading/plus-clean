﻿using System.Linq;
using System.Collections.Generic;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Catalog.Pets
{
    public class PetRaceManager
    {
        private List<PetRace> _races = new List<PetRace>();

        public void Init()
        {
            if (this._races.Count > 0)
                this._races.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `catalog_pet_races`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        PetRace Race = new PetRace(reader.GetInt32("raceid"), reader.GetInt32("color1"), reader.GetInt32("color2"), (reader.GetString("has1color") == "1"), (reader.GetString("has2color") == "1"));
                        if (!this._races.Contains(Race))
                            this._races.Add(Race);
                    }
            }
        }

        public List<PetRace> GetRacesForRaceId(int RaceId)
        {
            return this._races.Where(Race => Race.RaceId == RaceId).ToList();
        }
    }
}