﻿using Plus.HabboHotel.Items;

namespace Plus.HabboHotel.Catalog
{
    public class CatalogItem
    {
        public int Id { get; }
        public int ItemId { get; }
        public ItemData Data { get; }
        public int Amount { get; }
        public int CostCredits { get; }
        public string ExtraData { get; }
        public bool HaveOffer { get; }
        public bool IsLimited { get; }
        public string Name { get; }
        public int PageID { get; }
        public int CostPixels { get; }
        public int LimitedEditionStack { get; }
        public int LimitedEditionSells { get; set; }
        public int CostDiamonds { get; }
        public string Badge { get; }
        public int OfferId { get; }

        public CatalogItem(int Id, int ItemId, ItemData Data, string CatalogName, int PageId, int CostCredits, int CostPixels,
            int CostDiamonds, int Amount, int LimitedEditionSells, int LimitedEditionStack, bool HaveOffer, string ExtraData, string Badge, int OfferId)
        {
            this.Id = Id;
            this.Name = CatalogName;
            this.ItemId = ItemId;
            this.Data = Data;
            this.PageID = PageId;
            this.CostCredits = CostCredits;
            this.CostPixels = CostPixels;
            this.CostDiamonds = CostDiamonds;
            this.Amount = Amount;
            this.LimitedEditionSells = LimitedEditionSells;
            this.LimitedEditionStack = LimitedEditionStack;
            this.IsLimited = (LimitedEditionStack > 0);
            this.HaveOffer = HaveOffer;
            this.ExtraData = ExtraData;
            this.Badge = Badge;
            this.OfferId = OfferId;
        }
    }
}