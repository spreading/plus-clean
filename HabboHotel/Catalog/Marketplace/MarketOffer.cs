﻿namespace Plus.HabboHotel.Catalog.Marketplace
{
    public class MarketOffer
    {
        public int OfferID { get; }
        public int ItemType { get; }
        public int SpriteId { get; }
        public int TotalPrice { get; }
        public int LimitedNumber { get; }
        public int LimitedStack { get; }

        public MarketOffer(int OfferID, int SpriteId, int TotalPrice, int ItemType, int LimitedNumber, int LimitedStack)
        {
            this.OfferID = OfferID;
            this.SpriteId = SpriteId;
            this.ItemType = ItemType;
            this.TotalPrice = TotalPrice;
            this.LimitedNumber = LimitedNumber;
            this.LimitedStack = LimitedStack;
        }
    }
}
