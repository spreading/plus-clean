﻿using System;

namespace Plus.HabboHotel.Cache.Type
{
    public class UserCache
    {
        public int Id { get; }
        public string Username { get; }
        public string Motto { get; }
        public string Look { get; }
        public DateTime AddedTime { get; }
        public UserCache(int Id, string Username, string Motto, string Look)
        {
            this.Id = Id;
            this.Username = Username;
            this.Motto = Motto;
            this.Look = Look;
            this.AddedTime = DateTime.Now;
        }
        public bool IsExpired()
        {
            TimeSpan CacheTime = DateTime.Now - this.AddedTime;
            return CacheTime.TotalMinutes >= 30;
        }
    }
}
