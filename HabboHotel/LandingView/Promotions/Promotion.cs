﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.LandingView.Promotions
{
    public class Promotion
    {
        public int Id { get; }
        public string Title { get; }
        public string Text { get; }
        public string ButtonText { get; }
        public int ButtonType { get; }
        public string ButtonLink { get; }
        public string ImageLink { get; }

        public Promotion(int Id, string Title, string Text, string ButtonText, int ButtonType, string ButtonLink, string ImageLink)
        {
            this.Id = Id;
            this.Title = Title;
            this.Text = Text;
            this.ButtonText = ButtonText;
            this.ButtonType = ButtonType;
            this.ButtonLink = ButtonLink;
            this.ImageLink = ImageLink;
        }
    }
}
