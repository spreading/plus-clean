﻿using System.Collections.Generic;
using Plus.Database.Interfaces;
using Plus.HabboHotel.LandingView.Promotions;
using log4net;

namespace Plus.HabboHotel.LandingView
{
    public class LandingViewManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LandingViewManager));

        private Dictionary<int, Promotion> _promotionItems;

        public LandingViewManager()
        {
            this._promotionItems = new Dictionary<int, Promotion>();

            this.LoadPromotions();
        }

        public void LoadPromotions()
        {
            if (this._promotionItems.Count > 0)
                this._promotionItems.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `server_landing` ORDER BY `id` DESC");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _promotionItems.Add(reader.GetInt32("id"), new Promotion(reader.GetInt32("id"), reader.GetString("title"), reader.GetString("text"), reader.GetString("button_text"), reader.GetInt32("button_type"), reader.GetString("button_link"), reader.GetString("image_link")));
            }


            log.Info("Landing View Manager -> LOADED");
        }

        public ICollection<Promotion> GetPromotionItems()
        {
            return this._promotionItems.Values;
        }
    }
}