﻿using System.Collections.Generic;
using log4net;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Subscriptions
{
    public class SubscriptionManager
    {
        private static ILog log = LogManager.GetLogger(typeof(SubscriptionManager));

        private readonly Dictionary<int, SubscriptionData> _subscriptions = new Dictionary<int, SubscriptionData>();

        public SubscriptionManager()
        {
        }

        public void Init()
        {
            if (this._subscriptions.Count > 0)
                this._subscriptions.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `subscriptions`;");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        if (!this._subscriptions.ContainsKey(reader.GetInt32("id")))
                            this._subscriptions.Add(reader.GetInt32("id"), new SubscriptionData(reader.GetInt32("id"), reader.GetString("name"), reader.GetString("badge_code"), reader.GetInt32("credits"), reader.GetInt32("duckets"), reader.GetInt32("respects")));
            }

            log.Info("Loaded " + this._subscriptions.Count + " subscriptions.");
        }

        public bool TryGetSubscriptionData(int Id, out SubscriptionData Data)
        {
            return this._subscriptions.TryGetValue(Id, out Data);
        }
    }
}
