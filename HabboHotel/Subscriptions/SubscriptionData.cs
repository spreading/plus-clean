﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Subscriptions
{
    public class SubscriptionData
    {
        public int Id { get; }
        public string Name { get; }
        public string Badge { get; }
        public int Credits { get; }
        public int Duckets { get; }
        public int Respects { get; }

        public SubscriptionData(int Id, string Name, string Badge, int Credits, int Duckets, int Respects)
        {
            this.Id = Id;
            this.Name = Name;
            this.Badge = Badge;
            this.Credits = Credits;
            this.Duckets = Duckets;
            this.Respects = Respects;
        }
    }
}
