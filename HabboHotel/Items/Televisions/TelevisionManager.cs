﻿using System.Collections.Generic;
using Plus.Database.Interfaces;
using log4net;

namespace Plus.HabboHotel.Items.Televisions
{
    public class TelevisionManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(TelevisionManager));

        public Dictionary<int, TelevisionItem> _televisions;

        public TelevisionManager()
        {
            this._televisions =  new Dictionary<int, TelevisionItem>();

            this.Init();
        }

        public void Init()
        {
            if (this._televisions.Count > 0)
                _televisions.Clear();
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor()) 
            {
                dbClient.SetQuery("SELECT * FROM `items_youtube` ORDER BY `id` DESC");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _televisions.Add(reader.GetInt32("id"), new TelevisionItem(reader.GetInt32("id"), reader.GetString("youtube_id"), reader.GetString("title"), reader.GetString("description"), PlusEnvironment.EnumToBool(reader.GetString("enabled"))));
            }


            log.Info("Television Items -> LOADED");
        }


        public ICollection<TelevisionItem> TelevisionList
        {
            get
            {
                return this._televisions.Values;
            }
        }

        public bool TryGet(int ItemId, out TelevisionItem TelevisionItem)
        {
            if (this._televisions.TryGetValue(ItemId, out TelevisionItem))
                return true;
            return false;
        }
    }
}