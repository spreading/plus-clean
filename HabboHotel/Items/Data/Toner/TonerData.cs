﻿using Plus.Database.Interfaces;
using MySql.Data.MySqlClient;

namespace Plus.HabboHotel.Items.Data.Toner
{
    public class TonerData
    {
        public int ItemId;
        public int Hue;
        public int Saturation;
        public int Lightness;
        public int Enabled;

        public TonerData(int Item)
        {
            ItemId = Item;
            MySqlDataReader dataReader = null;

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT enabled,data1,data2,data3 FROM room_items_toner WHERE id=" + ItemId +" LIMIT 1");
                using (var reader = dbClient.ExecuteReader())
                    if (!reader.HasRows)
                    {
                        dbClient.RunQuery("INSERT INTO `room_items_toner` VALUES (" + ItemId + ",'0',0,0,0)");
                        dbClient.SetQuery("SELECT enabled,data1,data2,data3 FROM room_items_toner WHERE id=" + ItemId + " LIMIT 1");
                        using (var newDataReader = dbClient.ExecuteReader())
                            dataReader = newDataReader;
                    }
                    else
                    {
                        dataReader = reader;
                    }

                Enabled = int.Parse(dataReader.GetString("enabled"));
                Hue = dataReader.GetInt32("data1");
                Saturation = dataReader.GetInt32("data2");
                Lightness = dataReader.GetInt32("data3");
            }
        }
    }
}