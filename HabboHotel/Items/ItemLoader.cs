﻿using System.Collections.Generic;
using Plus.HabboHotel.Rooms;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Items
{
    public static class ItemLoader
    {
        public static List<Item> GetItemsForRoom(int RoomId, Room Room)
        {
            List<Item> I = new List<Item>();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `items`.*, COALESCE(`items_groups`.`group_id`, 0) AS `group_id` FROM `items` LEFT OUTER JOIN `items_groups` ON `items`.`id` = `items_groups`.`id` WHERE `items`.`room_id` = @rid;");
                dbClient.AddParameter("rid", RoomId);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        if (PlusEnvironment.Game.ItemManager.GetItem(reader.GetInt32("base_item"), out ItemData Data))
                        {
                            I.Add(new Item(reader.GetInt32("id"), reader.GetInt32("room_id"), reader.GetInt32("base_item"), reader.GetString("extra_data"),
                                reader.GetInt32("x"), reader.GetInt32("y"), reader.GetDouble("z"), reader.GetInt32("rot"), reader.GetInt32("user_id"),
                                reader.GetInt32("group_id"), reader.GetInt32("limited_number"), reader.GetInt32("limited_stack"), reader.GetString("wall_pos"), Room));
                        }
                        else
                        {
                            // Item data does not exist anymore.
                        }
                    }
            }
            return I;
        }

        public static List<Item> GetItemsForUser(int UserId)
        {
            List<Item> I = new List<Item>();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `items`.*, COALESCE(`items_groups`.`group_id`, 0) AS `group_id` FROM `items` LEFT OUTER JOIN `items_groups` ON `items`.`id` = `items_groups`.`id` WHERE `items`.`room_id` = 0 AND `items`.`user_id` = @uid;");
                dbClient.AddParameter("uid", UserId);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int baseItemId = reader.GetInt32("base_item");
                        if (PlusEnvironment.Game.ItemManager.GetItem(baseItemId, out ItemData Data))
                        {
                            I.Add(new Item(reader.GetInt32("id"), reader.GetInt32("room_id"), reader.GetInt32("base_item"), reader.GetString("extra_data"),
                                reader.GetInt32("x"), reader.GetInt32("y"), reader.GetDouble("z"), reader.GetInt32("rot"), reader.GetInt32("user_id"),
                                reader.GetInt32("group_id"), reader.GetInt32("limited_number"), reader.GetInt32("limited_stack"), reader.GetString("wall_pos")));
                        }
                    }
            }
            return I;
        }
    }
}