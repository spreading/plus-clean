﻿using System;
using System.Collections.Generic;
using log4net;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Items
{
    public class ItemDataManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ItemDataManager));

        public Dictionary<int, ItemData> _items;
        public Dictionary<int, ItemData> _gifts;//<SpriteId, Item>

        public ItemDataManager()
        {
            this._items = new Dictionary<int, ItemData>();
            this._gifts = new Dictionary<int, ItemData>();
        }

        public void Init()
        {
            if (this._items.Count > 0)
                this._items.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `furniture`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        try
                        {
                            int id = reader.GetInt32("id");
                            int spriteID = reader.GetInt32("sprite_id");
                            string itemName = reader.GetString("item_name");
                            string PublicName = reader.GetString("public_name");
                            string type = reader.GetString("type");
                            int width = reader.GetInt32("width");
                            int length = reader.GetInt32("length");
                            double height = reader.GetDouble("stack_height");
                            bool allowStack = PlusEnvironment.EnumToBool(reader.GetString("can_stack"));
                            bool allowWalk = PlusEnvironment.EnumToBool(reader.GetString("is_walkable"));
                            bool allowSit = PlusEnvironment.EnumToBool(reader.GetString("can_sit"));
                            bool allowRecycle = PlusEnvironment.EnumToBool(reader.GetString("allow_recycle"));
                            bool allowTrade = PlusEnvironment.EnumToBool(reader.GetString("allow_trade"));
                            bool allowMarketplace = reader.GetInt32("allow_marketplace_sell") == 1;
                            bool allowGift = reader.GetInt32("allow_gift") == 1;
                            bool allowInventoryStack = PlusEnvironment.EnumToBool(reader.GetString("allow_inventory_stack"));
                            InteractionType interactionType = InteractionTypes.GetTypeFromString(reader.GetString("interaction_type"));
                            int behaviourData = reader.GetInt32("behaviour_data");
                            int cycleCount = reader.GetInt32("interaction_modes_count");
                            string vendingIDS = reader.GetString("vending_ids");
                            string heightAdjustable = reader.GetString("height_adjustable");
                            int EffectId = reader.GetInt32("effect_id");
                            bool IsRare = PlusEnvironment.EnumToBool(reader.GetString("is_rare"));
                            bool ExtraRot = PlusEnvironment.EnumToBool(reader.GetString("extra_rot"));

                            if (!this._gifts.ContainsKey(spriteID))
                                this._gifts.Add(spriteID, new ItemData(id, spriteID, itemName, PublicName, type, width, length, height, allowStack, allowWalk, allowSit, allowRecycle, allowTrade, allowMarketplace, allowGift, allowInventoryStack, interactionType, behaviourData, cycleCount, vendingIDS, heightAdjustable, EffectId, IsRare, ExtraRot));

                            if (!this._items.ContainsKey(id))
                                this._items.Add(id, new ItemData(id, spriteID, itemName, PublicName, type, width, length, height, allowStack, allowWalk, allowSit, allowRecycle, allowTrade, allowMarketplace, allowGift, allowInventoryStack, interactionType, behaviourData, cycleCount, vendingIDS, heightAdjustable, EffectId, IsRare, ExtraRot));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.ToString());
                            Console.ReadKey();
                        }
                    }
            }

            log.Info("Item Manager -> LOADED");
        }

        public bool GetItem(int Id, out ItemData Item)
        {
            if (this._items.TryGetValue(Id, out Item))
                return true;
            return false;
        }

        public ItemData GetItemByName(string name)
        {
            foreach (var entry in _items)
            {
                ItemData item = entry.Value;
                if (item.ItemName == name)
                    return item;
            }
            return null;
        }

        public bool GetGift(int SpriteId, out ItemData Item)
        {
            if (this._gifts.TryGetValue(SpriteId, out Item))
                return true;
            return false;
        }
    }
}