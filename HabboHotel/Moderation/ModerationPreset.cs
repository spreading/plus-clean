﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Moderation
{
    class ModerationPreset
    {
        public int Id { get; }
        public string Type { get; }
        public string Message { get; }

        public ModerationPreset(int Id, string Type, string Message)
        {
            this.Id = Id;
            this.Type = Type;
            this.Message = Message;
        }
    }
}
