﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Moderation
{
    public class ModerationPresetActionMessages
    {
        public int Id { get; }
        public int ParentId { get; }
        public string Caption { get; }
        public string MessageText { get; }
        public int MuteTime { get; }
        public int BanTime { get; }
        public int IPBanTime { get; }
        public int TradeLockTime { get; }
        public string Notice { get; }

        public ModerationPresetActionMessages(int Id, int ParentId, string Caption, string MessageText, int MuteTime, int BanTime, int IPBanTime, int TradeLockTime, string Notice)
        {
            this.Id = Id;
            this.ParentId = ParentId;
            this.Caption = Caption;
            this.MessageText = MessageText;
            this.MuteTime = MuteTime;
            this.BanTime = BanTime;
            this.IPBanTime = IPBanTime;
            this.TradeLockTime = TradeLockTime;
            this.Notice = Notice;
        }
    }
}
