﻿using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;

using log4net;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Moderation
{
    public sealed class ModerationManager
    {
        private static ILog log = LogManager.GetLogger(typeof(ModerationManager));

        private int _ticketCount = 1;
        private List<string> _userPresets = new List<string>();
        private List<string> _roomPresets = new List<string>();
        private Dictionary<string, ModerationBan> _bans = new Dictionary<string, ModerationBan>();
        private Dictionary<int, string> _userActionPresetCategories = new Dictionary<int, string>();
        private Dictionary<int, List<ModerationPresetActionMessages>> _userActionPresetMessages = new Dictionary<int, List<ModerationPresetActionMessages>>();
        private ConcurrentDictionary<int, ModerationTicket> _modTickets = new ConcurrentDictionary<int, ModerationTicket>();


        private Dictionary<int, string> _moderationCFHTopics = new Dictionary<int, string>();
        private Dictionary<int, List<ModerationPresetActions>> _moderationCFHTopicActions = new Dictionary<int, List<ModerationPresetActions>>();

        public void Init()
        {
            if (this._userPresets.Count > 0)
                this._userPresets.Clear();
            if (this._moderationCFHTopics.Count > 0)
                this._moderationCFHTopics.Clear();
            if (this._moderationCFHTopicActions.Count > 0)
                this._moderationCFHTopicActions.Clear();
            if (this._bans.Count > 0)
                this._bans.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `moderation_presets`;");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        string Type = reader.GetString("type").ToLower();
                        switch (Type)
                        {
                            case "user":
                                this._userPresets.Add(reader.GetString("message"));
                                break;

                            case "room":
                                this._roomPresets.Add(reader.GetString("message"));
                                break;
                        }
                    }
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `moderation_topics`;");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        if (!this._moderationCFHTopics.ContainsKey(reader.GetInt32("id")))
                            this._moderationCFHTopics.Add(reader.GetInt32("id"), reader.GetString("caption"));
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `moderation_topic_actions`;");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int ParentId = reader.GetInt32("parent_id");

                        if (!this._moderationCFHTopicActions.ContainsKey(ParentId))
                        {
                            this._moderationCFHTopicActions.Add(ParentId, new List<ModerationPresetActions>());
                        }

                        this._moderationCFHTopicActions[ParentId].Add(new ModerationPresetActions(reader.GetInt32("id"), reader.GetInt32("parent_id"), reader.GetString("type"), reader.GetString("caption"), reader.GetString("message_text"),
                            reader.GetInt32("mute_time"), reader.GetInt32("ban_time"), reader.GetInt32("ip_time"), reader.GetInt32("trade_lock_time"), reader.GetString("default_sanction")));
                    }
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `moderation_preset_action_categories`;");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        this._userActionPresetCategories.Add(reader.GetInt32("id"), reader.GetString("caption"));
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `moderation_preset_action_messages`;");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int ParentId = reader.GetInt32("parent_id");

                        if (!this._userActionPresetMessages.ContainsKey(ParentId))
                        {
                            this._userActionPresetMessages.Add(ParentId, new List<ModerationPresetActionMessages>());
                        }

                        this._userActionPresetMessages[ParentId].Add(new ModerationPresetActionMessages(reader.GetInt32("id"), reader.GetInt32("parent_id"), reader.GetString("caption"), reader.GetString("message_text"),
                            reader.GetInt32("mute_hours"), reader.GetInt32("ban_hours"), reader.GetInt32("ip_ban_hours"), reader.GetInt32("trade_lock_days"), reader.GetString("notice")));
                    }
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `bantype`,`value`,`reason`,`expire` FROM `bans` WHERE `bantype` = 'machine' OR `bantype` = 'user'");

                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        string value = reader.GetString("value");
                        string reason = reader.GetString("reason");
                        double expires = reader.GetDouble("expire");
                        string type = reader.GetString("bantype");

                        ModerationBan Ban = new ModerationBan(BanTypeUtility.GetModerationBanType(type), value, reason, expires);
                        if (Ban != null)
                        {
                            if (expires > PlusEnvironment.GetUnixTimestamp())
                            {
                                if (!this._bans.ContainsKey(value))
                                    this._bans.Add(value, Ban);
                            }
                            else
                            {
                                dbClient.SetQuery("DELETE FROM `bans` WHERE `bantype` = '" + BanTypeUtility.FromModerationBanType(Ban.Type) + "' AND `value` = @Key LIMIT 1");
                                dbClient.AddParameter("Key", value);
                                dbClient.RunQuery();
                            }
                        }
                    }
            }

            log.Info("Loaded " + (this._userPresets.Count + this._roomPresets.Count) + " moderation presets.");
            log.Info("Loaded " + this._userActionPresetCategories.Count + " moderation categories.");
            log.Info("Loaded " + this._userActionPresetMessages.Count + " moderation action preset messages.");
            log.Info("Cached " + this._bans.Count + " username and machine bans.");
        }

        public void ReCacheBans()
        {
            if (this._bans.Count > 0)
                this._bans.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `bantype`,`value`,`reason`,`expire` FROM `bans` WHERE `bantype` = 'machine' OR `bantype` = 'user'");

                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        string value = reader.GetString("value");
                        string reason = reader.GetString("reason");
                        double expires = reader.GetDouble("expire");
                        string type = reader.GetString("bantype");

                        ModerationBan Ban = new ModerationBan(BanTypeUtility.GetModerationBanType(type), value, reason, expires);
                        if (Ban != null)
                        {
                            if (expires > PlusEnvironment.GetUnixTimestamp())
                            {
                                if (!this._bans.ContainsKey(value))
                                    this._bans.Add(value, Ban);
                            }
                            else
                            {
                                dbClient.SetQuery("DELETE FROM `bans` WHERE `bantype` = '" + BanTypeUtility.FromModerationBanType(Ban.Type) + "' AND `value` = @Key LIMIT 1");
                                dbClient.AddParameter("Key", value);
                                dbClient.RunQuery();
                            }
                        }
                    }
            }

            log.Info("Cached " + this._bans.Count + " username and machine bans.");
        }

        public void BanUser(string Mod, ModerationBanType Type, string BanValue, string Reason, double ExpireTimestamp)
        {
            string BanType = (Type == ModerationBanType.IP ? "ip" : Type == ModerationBanType.MACHINE ? "machine" : "user");
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("REPLACE INTO `bans` (`bantype`, `value`, `reason`, `expire`, `added_by`,`added_date`) VALUES ('" + BanType + "', '" + BanValue + "', @reason, " + ExpireTimestamp + ", '" + Mod + "', '" + PlusEnvironment.GetUnixTimestamp() + "');");
                dbClient.AddParameter("reason", Reason);
                dbClient.RunQuery();
            }

            if (Type == ModerationBanType.MACHINE || Type == ModerationBanType.USERNAME)
            {
                if (!this._bans.ContainsKey(BanValue))
                    this._bans.Add(BanValue, new ModerationBan(Type, BanValue, Reason, ExpireTimestamp));
            }
        }

        public ICollection<string> UserMessagePresets
        {
            get { return this._userPresets; }
        }

        public ICollection<string> RoomMessagePresets
        {
            get { return this._roomPresets; }
        }

        public ICollection<ModerationTicket> GetTickets
        {
            get { return this._modTickets.Values; }
        }

        public Dictionary<string, List<ModerationPresetActions>> UserActionPresets
        {
            get
            {
                Dictionary<string, List<ModerationPresetActions>> Result = new Dictionary<string, List<ModerationPresetActions>>();
                foreach (KeyValuePair<int, string> Category in this._moderationCFHTopics.ToList())
                {
                    Result.Add(Category.Value, new List<ModerationPresetActions>());

                    if (this._moderationCFHTopicActions.ContainsKey(Category.Key))
                    {
                        foreach (ModerationPresetActions Data in this._moderationCFHTopicActions[Category.Key])
                        {
                            Result[Category.Value].Add(Data);
                        }
                    }
                }
                return Result;
            }
        }

        public bool TryAddTicket(ModerationTicket Ticket)
        {
            Ticket.Id = this._ticketCount++;
            return this._modTickets.TryAdd(Ticket.Id, Ticket);
        }

        public bool TryGetTicket(int TicketId, out ModerationTicket Ticket)
        {
            return this._modTickets.TryGetValue(TicketId, out Ticket);
        }

        public bool UserHasTickets(int userId)
        {
            return this._modTickets.Count(x => x.Value.Sender.Id == userId && x.Value.Answered == false) > 0;
        }

        public ModerationTicket GetTicketBySenderId(int userId)
        {
            return this._modTickets.FirstOrDefault(x => x.Value.Sender.Id == userId).Value;
        }

        /// <summary>
        /// Runs a quick check to see if a ban record is cached in the server.
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Ban"></param>
        /// <returns></returns>
        public bool IsBanned(string Key, out ModerationBan Ban)
        {
            if (this._bans.TryGetValue(Key, out Ban))
            {
                if (!Ban.Expired)
                    return true;

                //This ban has expired, let us quickly remove it here.
                using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                {
                    dbClient.SetQuery("DELETE FROM `bans` WHERE `bantype` = '" + BanTypeUtility.FromModerationBanType(Ban.Type) + "' AND `value` = @Key LIMIT 1");
                    dbClient.AddParameter("Key", Key);
                    dbClient.RunQuery();
                }

                //And finally, let us remove the ban record from the cache.
                if (this._bans.ContainsKey(Key))
                    this._bans.Remove(Key);
                return false;
            }
            return false;
        }

        /// <summary>
        /// Run a quick database check to see if this ban exists in the database.
        /// </summary>
        /// <param name="MachineId">The value of the ban.</param>
        /// <returns></returns>
        public bool MachineBanCheck(string MachineId)
        {
            if (PlusEnvironment.Game.ModerationManager.IsBanned(MachineId, out ModerationBan MachineBanRecord))
            {
                using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                {
                    dbClient.SetQuery("SELECT * FROM `bans` WHERE `bantype` = 'machine' AND `value` = @value LIMIT 1");
                    dbClient.AddParameter("value", MachineId);

                    using (var reader = dbClient.ExecuteReader())
                        if (!reader.HasRows)
                        {
                            PlusEnvironment.Game.ModerationManager.RemoveBan(MachineId);
                            return false;
                        }
                }
            }
            return true;
        }

        /// <summary>
        /// Run a quick database check to see if this ban exists in the database.
        /// </summary>
        /// <param name="Username">The value of the ban.</param>
        /// <returns></returns>
        public bool UsernameBanCheck(string Username)
        {
            if (PlusEnvironment.Game.ModerationManager.IsBanned(Username, out ModerationBan UsernameBanRecord))
            {
                using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                {
                    dbClient.SetQuery("SELECT * FROM `bans` WHERE `bantype` = 'user' AND `value` = @value LIMIT 1");
                    dbClient.AddParameter("value", Username);
                    using (var reader = dbClient.ExecuteReader())
                        if (!reader.HasRows)
                        {
                            PlusEnvironment.Game.ModerationManager.RemoveBan(Username);
                            return false;
                        }
                }
            }
            return true;
        }

        /// <summary>
        /// Remove a ban from the cache based on a given value.
        /// </summary>
        /// <param name="Value"></param>
        public void RemoveBan(string Value)
        {
            this._bans.Remove(Value);
        }
    }
}
