﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Moderation
{
    public class ModerationPresetActions
    {
        public int Id { get; }
        public int ParentId { get; }
        public string Type { get; }
        public string Caption { get; }
        public string MessageText { get; }
        public int MuteTime { get; }
        public int BanTime { get; }
        public int IPBanTime { get; }
        public int TradeLockTime { get; }
        public string DefaultSanction { get; }

        public ModerationPresetActions(int id, int parentId, string type, string caption, string messageText, int muteText, int banTime, int ipBanTime, int tradeLockTime, string defaultSanction)
        {
            this.Id = id;
            this.ParentId = parentId;
            this.Type = type;
            this.Caption = caption;
            this.MessageText = messageText;
            this.MuteTime = muteText;
            this.BanTime = banTime;
            this.IPBanTime = ipBanTime;
            this.TradeLockTime = tradeLockTime;
            this.DefaultSanction = defaultSanction;
        }
    }
}
