﻿using System.Linq;
using System.Collections.Generic;
using log4net;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.AI.Responses;
using Plus.HabboHotel.Rooms.AI;

namespace Plus.HabboHotel.Bots
{
    public class BotManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BotManager));

        private readonly List<BotResponse> _responses;

        public BotManager()
        {
            this._responses = new List<BotResponse>();
        }

        public void Init()
        {
            BotDao.LoadBotResponses(_responses);
        }

        public BotResponse GetResponse(BotAIType AiType, string Message)
        {
            foreach (BotResponse Response in this._responses.Where(X => X.AiType == AiType).ToList())
            {
                if (Response.KeywordMatched(Message))
                {
                    return Response;
                }
            }

            return null;
        }
    }
}