﻿using log4net;

using Plus.Communication.Packets;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Moderation;
using Plus.HabboHotel.Catalog;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Items.Televisions;
using Plus.HabboHotel.Navigator;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.Quests;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.LandingView;
using Plus.HabboHotel.Games;
using Plus.HabboHotel.Rooms.Chat;
using Plus.HabboHotel.Talents;
using Plus.HabboHotel.Bots;
using Plus.HabboHotel.Cache;
using Plus.HabboHotel.Rewards;
using Plus.HabboHotel.Badges;
using Plus.HabboHotel.Permissions;
using Plus.HabboHotel.Subscriptions;
using System.Threading;
using System.Threading.Tasks;
using Plus.Core;

namespace Plus.HabboHotel
{
    public class Game
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Game));

        public AchievementManager   AchievementManager { get; }
        public BadgeManager         BadgeManager { get; }
        public BotManager           BotManager { get; }
        public CacheManager         CacheManager { get; }
        public CatalogManager       CatalogManager { get; }
        public ChatManager          ChatManager { get; }
        public GameClientManager    ClientManager { get; }
        public GameDataManager      GameDataManager { get; }
        public GroupManager         GroupManager { get; }
        public ItemDataManager      ItemManager { get; }
        public LandingViewManager   LandingManager { get; }
        public ModerationManager    ModerationManager { get; }
        public NavigatorManager     NavigatorManager { get; }
        public PacketManager        PacketManager { get; }
        public PermissionManager    PermissionManager { get; }
        public QuestManager         QuestManager { get; }
        public RewardManager        RewardManager { get; }
        public RoomManager          RoomManager { get; }
        public SubscriptionManager  SubscriptionManager { get; }
        public TalentTrackManager   TalentTrackManager { get; }
        public TelevisionManager    TelevisionManager { get; }//TODO: Initialize from the item manager.

        private readonly ServerStatusUpdater _globalUpdater;

        private bool _cycleEnded;
        private bool _cycleActive;
        private Task _gameCycle;
        private int _cycleSleepTime = 25;

        public Game()
        {
            this.PacketManager = new PacketManager();
            this.ClientManager = new GameClientManager();

            this.ModerationManager = new ModerationManager();
            this.ModerationManager.Init();

            this.ItemManager = new ItemDataManager();
            this.ItemManager.Init();

            this.CatalogManager = new CatalogManager();
            this.CatalogManager.Init(this.ItemManager);

            this.TelevisionManager = new TelevisionManager();

            this.NavigatorManager = new NavigatorManager();
            this.RoomManager = new RoomManager();
            this.ChatManager = new ChatManager();
            this.GroupManager = new GroupManager();
            this.GroupManager.Init();
            this.QuestManager = new QuestManager();
            this.AchievementManager = new AchievementManager();
            this.TalentTrackManager = new TalentTrackManager();

            this.LandingManager = new LandingViewManager();
            this.GameDataManager = new GameDataManager();

            this._globalUpdater = new ServerStatusUpdater();
            this._globalUpdater.Init();
            
            this.BotManager = new BotManager();
            this.BotManager.Init();

            this.CacheManager = new CacheManager();
            this.RewardManager = new RewardManager();

            this.BadgeManager = new BadgeManager();
            this.BadgeManager.Init();

            this.PermissionManager = new PermissionManager();
            this.PermissionManager.Init();

            this.SubscriptionManager = new SubscriptionManager();
            this.SubscriptionManager.Init();

        }

        public void StartGameLoop()
        {
            this._gameCycle = new Task(GameCycle);
            this._gameCycle.Start();

            this._cycleActive = true;
        }

        private void GameCycle()
        {
            while (this._cycleActive)
            {
                this._cycleEnded = false;

                RoomManager.OnCycle();
                ClientManager.OnCycle();

                this._cycleEnded = true;
                Thread.Sleep(this._cycleSleepTime);
            }
        }

        public void StopGameLoop()
        {
            this._cycleActive = false;

            while (!this._cycleEnded)
            {
                Thread.Sleep(this._cycleSleepTime);
            }
        }
    }
}