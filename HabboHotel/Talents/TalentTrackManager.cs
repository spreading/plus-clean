﻿using System.Collections.Generic;
using log4net;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Talents
{
    public class TalentTrackManager
    {
        private static ILog log = LogManager.GetLogger(typeof(TalentTrackManager));

        private readonly Dictionary<int, TalentTrackLevel> _citizenshipLevels;

        public TalentTrackManager()
        {
            this._citizenshipLevels = new Dictionary<int, TalentTrackLevel>();

            this.Init();
        }

        public void Init()
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `type`,`level`,`data_actions`,`data_gifts` FROM `talents`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _citizenshipLevels.Add(reader.GetInt32("level"), new TalentTrackLevel(reader.GetString("type"), reader.GetInt32("level"), reader.GetString("data_actions"), reader.GetString("data_gifts")));
            }
        }

        public ICollection<TalentTrackLevel> GetLevels()
        {
            return this._citizenshipLevels.Values;
        }
    }
}
