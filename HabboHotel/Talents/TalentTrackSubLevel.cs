﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Plus.HabboHotel.Achievements;

namespace Plus.HabboHotel.Talents
{
    public class TalentTrackSubLevel
    {
        public int Level { get; }
        public string Badge { get; }
        public int RequiredProgress { get; }

        public TalentTrackSubLevel(int Level, string Badge, int RequiredProgress)
        {
            this.Level = Level;
            this.Badge = Badge;
            this.RequiredProgress = RequiredProgress;
        }
    }
}
