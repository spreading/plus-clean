﻿using System.Collections.Generic;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Talents
{
    public class TalentTrackLevel
    {
        public string Type { get; }
        public int Level { get; }

        private List<string> _dataActions;
        private List<string> _dataGifts;

        private Dictionary<int, TalentTrackSubLevel> _subLevels;

        public TalentTrackLevel(string Type, int Level, string DataActions, string DataGifts)
        {
            this.Type = Type;
            this.Level = Level;

            foreach (string Str in DataActions.Split('|'))
            {
                if (this._dataActions == null) { this._dataActions = new List<string>(); }
                this._dataActions.Add(Str);
            }

            foreach (string Str in DataGifts.Split('|'))
            {
                if (this._dataGifts == null) { this._dataGifts = new List<string>(); }
                this._dataGifts.Add(Str);
            }

            this._subLevels = new Dictionary<int, TalentTrackSubLevel>();

            this.Init();
        }

        public List<string> Actions
        {
            get { return this._dataActions; }
            private set { this._dataActions = value; }
        }

        public List<string> Gifts
        {
            get { return this._dataGifts; }
            private set { this._dataGifts = value; }
        }

        public void Init()
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `sub_level`,`badge_code`,`required_progress` FROM `talents_sub_levels` WHERE `talent_level` = @TalentLevel");
                dbClient.AddParameter("TalentLevel", this.Level);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _subLevels.Add(reader.GetInt32("sub_level"), new TalentTrackSubLevel(reader.GetInt32("sub_level"), reader.GetString("badge_code"), reader.GetInt32("required_progress")));
            }
        }

        public ICollection<TalentTrackSubLevel> GetSubLevels()
        {
            return this._subLevels.Values;
        }
    }
}
