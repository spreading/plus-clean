﻿using System;
using Plus.Core;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Sound;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.Communication.Packets.Outgoing.Inventory.AvatarEffects;
using Plus.Communication.Packets.Outgoing.Inventory.Achievements;
using Plus.Communication.Encryption.Crypto.Prng;
using Plus.HabboHotel.Users.Messenger.FriendBar;
using Plus.Communication.Packets.Outgoing.BuildersClub;
using Plus.HabboHotel.Moderation;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Permissions;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.HabboHotel.Users.UserData;
using DotNetty.Transport.Channels;
using Plus.Communication.Packets.Outgoing;

namespace Plus.HabboHotel.GameClients
{
    public class GameClient
    {
        private Habbo _habbo;
        public string MachineId;
        private bool _disconnected;
        public ARC4 RC4Client = null;
        public int PingCount { get; set; }
        private IChannelHandlerContext channel;

        public GameClient(IChannelHandlerContext context)
        {
            channel = context;
        }

        public bool TryAuthenticate(string AuthTicket)
        {
            try
            {
                UserData userData = UserDataFactory.GetUserData(AuthTicket, out byte errorCode);

                if (errorCode == 1 || errorCode == 2)
                {
                    Disconnect();
                    return false;
                }

                #region Ban Checking
                //Let's have a quick search for a ban before we successfully authenticate..
                ModerationBan BanRecord = null;
                if (!string.IsNullOrEmpty(MachineId))
                {
                    if (PlusEnvironment.Game.ModerationManager.IsBanned(MachineId, out BanRecord))
                    {
                        if (PlusEnvironment.Game.ModerationManager.MachineBanCheck(MachineId))
                        {
                            Disconnect();
                            return false;
                        }
                    }
                }

                if (userData.user != null)
                {
                    //Now let us check for a username ban record..
                    BanRecord = null;
                    if (PlusEnvironment.Game.ModerationManager.IsBanned(userData.user.Username, out BanRecord))
                    {
                        if (PlusEnvironment.Game.ModerationManager.UsernameBanCheck(userData.user.Username))
                        {
                            Disconnect();
                            return false;
                        }
                    }
                }
                #endregion

                PlusEnvironment.Game.ClientManager.RegisterClient(this, userData.userID, userData.user.Username);
                _habbo = userData.user;
                if (_habbo != null)
                {
                    _habbo.Init(this, userData);

                    SendPacket(new AuthenticationOKComposer());
                    SendPacket(new AvatarEffectsComposer(_habbo.Effects.GetAllEffects));
                    SendPacket(new NavigatorSettingsComposer(_habbo.HomeRoom));
                    SendPacket(new FavouritesComposer(userData.user.FavoriteRooms));
                    SendPacket(new FigureSetIdsComposer(_habbo.Clothing.GetClothingParts));
                    SendPacket(new UserRightsComposer(_habbo.Rank));
                    SendPacket(new AvailabilityStatusComposer());
                    SendPacket(new AchievementScoreComposer(_habbo.Stats.AchievementPoints));
                    SendPacket(new BuildersClubMembershipComposer());
                    SendPacket(new CfhTopicsInitComposer(PlusEnvironment.Game.ModerationManager.UserActionPresets));

                    SendPacket(new BadgeDefinitionsComposer(PlusEnvironment.Game.AchievementManager._achievements));
                    SendPacket(new SoundSettingsComposer(_habbo.ClientVolume, _habbo.ChatPreference, _habbo.AllowMessengerInvites, _habbo.FocusPreference, FriendBarStateUtility.GetInt(_habbo.FriendbarState)));
                    //SendMessage(new TalentTrackLevelComposer());

                    if (GetHabbo().Messenger != null)
                        GetHabbo().Messenger.OnStatusChanged(true);

                    if (!string.IsNullOrEmpty(MachineId))
                    {
                        if (this._habbo.MachineId != MachineId)
                        {
                            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                            {
                                dbClient.SetQuery("UPDATE `users` SET `machine_id` = @MachineId WHERE `id` = @id LIMIT 1");
                                dbClient.AddParameter("MachineId", MachineId);
                                dbClient.AddParameter("id", _habbo.Id);
                                dbClient.RunQuery();
                            }
                        }

                        _habbo.MachineId = MachineId;
                    }

                    if (PlusEnvironment.Game.PermissionManager.TryGetGroup(_habbo.Rank, out PermissionGroup PermissionGroup))
                    {
                        if (!String.IsNullOrEmpty(PermissionGroup.Badge))
                            if (!_habbo.BadgeComponent.HasBadge(PermissionGroup.Badge))
                                _habbo.BadgeComponent.GiveBadge(PermissionGroup.Badge, true, this);
                    }

                    if (PlusEnvironment.Game.SubscriptionManager.TryGetSubscriptionData(this._habbo.VIPRank, out SubscriptionData SubData))
                    {
                        if (!String.IsNullOrEmpty(SubData.Badge))
                        {
                            if (!_habbo.BadgeComponent.HasBadge(SubData.Badge))
                                _habbo.BadgeComponent.GiveBadge(SubData.Badge, true, this);
                        }
                    }

                    if (!PlusEnvironment.Game.CacheManager.ContainsUser(_habbo.Id))
                        PlusEnvironment.Game.CacheManager.GenerateUser(_habbo.Id);

                    _habbo.Look = PlusEnvironment.FigureManager.ProcessFigure(this._habbo.Look, this._habbo.Gender, this._habbo.Clothing.GetClothingParts, true);
                    _habbo.InitProcess();
          
                    if (userData.user.Permissions.HasRight("mod_tickets"))
                    {
                        SendPacket(new ModeratorInitComposer(
                          PlusEnvironment.Game.ModerationManager.UserMessagePresets,
                          PlusEnvironment.Game.ModerationManager.RoomMessagePresets,
                          PlusEnvironment.Game.ModerationManager.GetTickets));
                    }

                    if (PlusEnvironment.SettingsManager.TryGetValue("user.login.message.enabled") == "1")
                        SendPacket(new MOTDNotificationComposer(PlusEnvironment.LanguageManager.TryGetValue("user.login.message")));

                    PlusEnvironment.Game.RewardManager.CheckRewards(this);
                    return true;
                }
            }
            catch (Exception e)
            {
                ExceptionLogger.LogException(e);
            }
            return false;
        }

        public void SendWhisper(string Message, int Colour = 0)
        {
            if (GetHabbo() == null || GetHabbo().CurrentRoom == null)
                return;

            RoomUser User = GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(GetHabbo().Username);
            if (User == null)
                return;

            SendPacket(new WhisperComposer(User.VirtualId, Message, 0, (Colour == 0 ? User.LastBubble : Colour)));
        }

        public void SendNotification(string Message)
        {
            SendPacket(new BroadcastMessageAlertComposer(Message));
        }

        public void SendPacket(ServerPacket packet)
        {
            channel.WriteAndFlushAsync(packet);
        }

        public Habbo GetHabbo()
        {
            return _habbo;
        }

        public void Disconnect()
        {
            try
            {
                if (GetHabbo() != null)
                {
                    using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                    {
                        dbClient.RunQuery(GetHabbo().GetQueryString);
                    }

                    GetHabbo().OnDisconnect();
                }
            }
            catch (Exception e)
            {
                ExceptionLogger.LogException(e);
            }

            if (!_disconnected)
            {
                if (channel != null)
                    channel.CloseAsync();
                _disconnected = true;
            }
        }

        public void Dispose()
        {
            if (GetHabbo() != null)
                GetHabbo().OnDisconnect();

            this.MachineId = string.Empty;
            this._disconnected = true;
            this._habbo = null;
            this.channel.DisconnectAsync();
            this.RC4Client = null;
        }
    }
}