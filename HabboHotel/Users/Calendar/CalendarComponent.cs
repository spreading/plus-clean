﻿using System.Collections.Generic;

using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Users.Calendar
{
    /// <summary>
    /// Permissions for a specific Player.
    /// </summary>
    public sealed class CalendarComponent
    {
        /// <summary>
        /// Permission rights are stored here.
        /// </summary>
        private readonly List<int> _lateBoxes;

        private readonly List<int> _openedBoxes;

        public CalendarComponent()
        {
            this._lateBoxes = new List<int>();
            this._openedBoxes = new List<int>();
        }

        /// <summary>
        /// Initialize the PermissionComponent.
        /// </summary>
        /// <param name="Player"></param>
        public bool Init(Habbo Player)
        {
            if (this._lateBoxes.Count > 0)
                this._lateBoxes.Clear();

            if (this._openedBoxes.Count > 0)
                this._openedBoxes.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `user_xmas15_calendar` WHERE `user_id` = @id;");
                dbClient.AddParameter("id", Player.Id);

                using (var reader = dbClient.ExecuteReader())
                    while(reader.Read())
                        if (reader.GetInt32("status") == 0)
                        {
                            this._lateBoxes.Add(reader.GetInt32("day"));
                        }
                        else
                        {
                            this._openedBoxes.Add(reader.GetInt32("day"));
                        }
            }
            return true;
        }

        public List<int> GetOpenedBoxes()
        {
            return this._openedBoxes;
        }

        public List<int> GetLateBoxes()
        {
            return this._lateBoxes;
        }

        /// <summary>
        /// Dispose of the permissions list.
        /// </summary>
        public void Dispose()
        {
            this._lateBoxes.Clear();
            this._openedBoxes.Clear();
        }
    }
}
