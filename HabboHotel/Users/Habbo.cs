﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using log4net;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.Users.Badges;
using Plus.HabboHotel.Users.Inventory;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;
using Plus.HabboHotel.Users.Process;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.HabboHotel.Users.Effects;
using Plus.HabboHotel.Users.Messenger.FriendBar;
using Plus.HabboHotel.Users.Clothing;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Chat.Commands;
using Plus.HabboHotel.Users.Permissions;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users.Ignores;
using MySql.Data.MySqlClient;

namespace Plus.HabboHotel.Users
{
    public class Habbo
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Habbo));

        //Generic player values.
        public int Id { get; set; }
        public string Username { get; private set; }
        public int Rank { get; set; }
        public string Motto { get; set; }
        public string Look { get; set; }
        public string Gender { get; set; }
        public string FootballLook { get; set; }
        public string FootballGender { get; set; }
        public int Credits { get; set; }
        public int Duckets { get; set; }
        public int Diamonds { get; set; }
        public int GOTWPoints { get; set; }
        public int HomeRoom { get; set; }
        public double LastOnline { get; set; }
        public double AccountCreated { get; set; }
        public List<int> ClientVolume { get; set; }
        public double LastNameChange { get; set; }
        public string MachineId { get; set; }
        public bool ChatPreference { get; set; }
        public bool FocusPreference { get; set; }
        public bool IsExpert { get; set; }
        public int VIPRank { get; set; }

        //Abilitys triggered by generic events.
        public bool AppearOffline { get; set; }
        public bool AllowTradingRequests { get; set; }
        public bool AllowUserFollowing { get; set; }
        public bool AllowFriendRequests { get; set; }
        public bool AllowMessengerInvites { get; set; }
        public bool AllowPetSpeech { get; set; }
        public bool AllowBotSpeech { get; set; }
        public bool AllowPublicRoomStatus { get; set; }
        public bool AllowConsoleMessages { get; set; }
        public bool AllowGifts { get; set; }
        public bool AllowMimic { get; set; }
        public bool ReceiveWhispers { get; set; }
        public bool IgnorePublicWhispers { get; set; }
        public bool PlayingFastFood { get; set; }
        public FriendBarState FriendbarState { get; set; }
        public int ChristmasDay { get; set; }
        public int WantsToRideHorse { get; set; }
        public int TimeAFK { get; set; }
        public bool DisableForcedEffects { get; set; }

        //Player saving.
        private bool _disconnected;
        private bool _habboSaved;
        public bool ChangingName { get; set; }

        //Counters
        public double FloodTime { get; set; }
        public int FriendCount { get; private set; }
        public double TimeMuted { get; set; }
        public double TradingLockExpiry { get; set; }
        public int BannedPhraseCount { get; set; }
        public double SessionStart { get; set; }
        public int MessengerSpamCount { get; set; }
        public double MessengerSpamTime { get; set; }
        public int CreditsUpdateTick { get; private set; }

        //Room related
        public int TentId { get; set; }
        public int HopperId { get; set; }
        public bool IsHopping { get; set; }
        public int TeleporterId { get; set; }
        public bool IsTeleporting { get; set; }
        public int TeleportingRoomID { get; set; }
        public bool RoomAuthOk { get; set; }
        public int CurrentRoomId { get; set; }

        //Advertising reporting system.
        public bool HasSpoken { get; set; }
        public bool AdvertisingReported { get; set; }
        public double LastAdvertiseReport { get; set; }
        public bool AdvertisingReportedBlocked { get; set; }

        //Values generated within the game.
        public bool WiredInteraction { get; set; }
        public int QuestLastCompleted { get; set; }
        public bool InventoryAlert { get; set; }
        public bool IgnoreBobbaFilter { get; set; }
        public bool WiredTeleporting { get; set; }
        public int CustomBubbleId { get; set; }
        public bool OnHelperDuty { get; set; }

        //Fastfood
        public int FastfoodScore { get; set; }

        //Just random fun stuff.
        public int PetId { get; set; }

        //Anti-script placeholders.
        public DateTime LastGiftPurchaseTime { get; set; }
        public DateTime LastMottoUpdateTime { get; set; }
        public DateTime LastClothingUpdateTime { get; set; }
        public DateTime LastForumMessageUpdateTime { get; set; }

        public int GiftPurchasingWarnings { get; set; }
        public int MottoUpdateWarnings { get; set; }
        public int ClothingUpdateWarnings { get; set; }

        public bool SessionGiftBlocked { get; set; }
        public bool SessionMottoBlocked { get; set; }
        public bool SessionClothingBlocked { get; set; }

        public List<int> RatedRooms;
        public List<RoomData> UsersRooms;

        private GameClient _client;
        public HabboStats Stats { get; set; }
        public HabboMessenger Messenger { get; private set; }
        private ProcessComponent _process;
        public ArrayList FavoriteRooms { get; private set; }
        public Dictionary<int, int> Quests { get; private set; }
        public BadgeComponent BadgeComponent { get; private set; }
        public InventoryComponent InventoryComponent { get; private set; }
        public Dictionary<int, Relationship> Relationships;
        public ConcurrentDictionary<string, UserAchievement> Achievements;

        private DateTime _timeCached;

        public SearchesComponent NavigatorSearches { get; private set; }
        public EffectsComponent Effects { get; private set; }
        public ClothingComponent Clothing { get; private set; }
        public PermissionComponent Permissions { get; private set; }
        public IgnoresComponent Ignores { get; private set; }

        public IChatCommand IChatCommand { get; set; }

        public Habbo(MySqlDataReader reader)
        {
            this.Id = reader.GetInt32("id");
            this.Username = reader.GetString("username");
            this.Rank = reader.GetInt32("rank");
            this.Motto = reader.GetString("motto");
            this.Look = reader.GetString("look");
            this.Gender = reader.GetString("gender");
            this.FootballLook = PlusEnvironment.FilterFigure(Look.ToLower());
            this.FootballGender = Gender.ToLower();
            this.Credits = reader.GetInt32("credits");
            this.Duckets = reader.GetInt32("activity_points");
            this.Diamonds = reader.GetInt32("vip_points");
            this.GOTWPoints = reader.GetInt32("gotw_points");
            this.HomeRoom = reader.GetInt32("home_room");
            this.LastOnline = reader.GetInt32("last_online");
            this.AccountCreated = reader.GetDouble("account_created");
            this.ClientVolume = new List<int>();
            string clientVolume = reader.GetString("volume");
            foreach (string Str in clientVolume.Split(','))
            {
                if (int.TryParse(Str, out int Val))
                    this.ClientVolume.Add(int.Parse(Str));
                else
                    this.ClientVolume.Add(100);
            }

            this.LastNameChange = reader.GetDouble("last_change");
            this.MachineId = reader.GetString("machine_id");
            this.ChatPreference = PlusEnvironment.EnumToBool(reader.GetString("chat_preference"));
            this.FocusPreference = PlusEnvironment.EnumToBool(reader.GetString("focus_preference"));
            this.IsExpert = IsExpert == true;

            this.AppearOffline = PlusEnvironment.EnumToBool(reader.GetString("hide_online"));
            this.AllowTradingRequests = true;//TODO
            this.AllowUserFollowing = true;//TODO
            this.AllowFriendRequests = PlusEnvironment.EnumToBool(reader.GetString("block_newfriends"));//TODO
            this.AllowMessengerInvites = PlusEnvironment.EnumToBool(reader.GetString("ignore_invites"));
            this.AllowPetSpeech = PlusEnvironment.EnumToBool(reader.GetString("pets_muted"));
            this.AllowBotSpeech = PlusEnvironment.EnumToBool(reader.GetString("bots_muted"));
            this.AllowPublicRoomStatus = PlusEnvironment.EnumToBool(reader.GetString("hide_inroom"));
            this.AllowConsoleMessages = true;
            this.AllowGifts = PlusEnvironment.EnumToBool(reader.GetString("allow_gifts"));
            this.AllowMimic = PlusEnvironment.EnumToBool(reader.GetString("allow_mimic"));
            this.ReceiveWhispers = true;
            this.IgnorePublicWhispers = false;
            this.PlayingFastFood = false;
            this.FriendbarState = FriendBarStateUtility.GetEnum(reader.GetInt32("friend_bar_state"));
            this.ChristmasDay = ChristmasDay;
            this.WantsToRideHorse = 0;
            this.TimeAFK = 0;
            this.DisableForcedEffects = PlusEnvironment.EnumToBool(reader.GetString("disable_forced_effects"));
            this.VIPRank = reader.GetInt32("rank_vip");

            this._disconnected = false;
            this._habboSaved = false;
            this.ChangingName = false;

            this.FloodTime = 0;
            this.FriendCount = 0;
            this.TimeMuted = reader.GetDouble("time_muted");
            this._timeCached = DateTime.Now;

            this.TradingLockExpiry = reader.GetDouble("trading_locked");
            if (this.TradingLockExpiry > 0 && PlusEnvironment.GetUnixTimestamp() > this.TradingLockExpiry)
            {
                this.TradingLockExpiry = 0;
                using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                    dbClient.RunQuery("UPDATE `user_info` SET `trading_locked` = '0' WHERE `user_id` = '" + Id + "' LIMIT 1");
            }

            this.BannedPhraseCount = 0;
            this.SessionStart = PlusEnvironment.GetUnixTimestamp();
            this.MessengerSpamCount = 0;
            this.MessengerSpamTime = 0;
            this.CreditsUpdateTick = Convert.ToInt32(PlusEnvironment.SettingsManager.TryGetValue("user.currency_scheduler.tick"));

            this.TentId = 0;
            this.HopperId = 0;
            this.IsHopping = false;
            this.TeleporterId = 0;
            this.IsTeleporting = false;
            this.TeleportingRoomID = 0;
            this.RoomAuthOk = false;
            this.CurrentRoomId = 0;

            this.HasSpoken = false;
            this.LastAdvertiseReport = 0;
            this.AdvertisingReported = false;
            this.AdvertisingReportedBlocked = PlusEnvironment.EnumToBool(reader.GetString("advertising_report_blocked"));

            this.WiredInteraction = false;
            this.QuestLastCompleted = 0;
            this.InventoryAlert = false;
            this.IgnoreBobbaFilter = false;
            this.WiredTeleporting = false;
            this.CustomBubbleId = 0;
            this.OnHelperDuty = false;
            this.FastfoodScore = 0;
            this.PetId = 0;

            this.LastGiftPurchaseTime = DateTime.Now;
            this.LastMottoUpdateTime = DateTime.Now;
            this.LastClothingUpdateTime = DateTime.Now;
            this.LastForumMessageUpdateTime = DateTime.Now;

            this.GiftPurchasingWarnings = 0;
            this.MottoUpdateWarnings = 0;
            this.ClothingUpdateWarnings = 0;

            this.SessionGiftBlocked = false;
            this.SessionMottoBlocked = false;
            this.SessionClothingBlocked = false;

            this.FavoriteRooms = new ArrayList();
            this.Achievements = new ConcurrentDictionary<string, UserAchievement>();
            this.Relationships = new Dictionary<int, Relationship>();
            this.RatedRooms = new List<int>();
            this.UsersRooms = new List<RoomData>();

            //TODO: Nope.
            this.InitPermissions();

            #region Stats
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                bool hasStats = true;
                dbClient.SetQuery("SELECT `id`,`roomvisits`,`onlinetime`,`respect`,`respectgiven`,`giftsgiven`,`giftsreceived`,`dailyrespectpoints`,`dailypetrespectpoints`,`achievementscore`,`quest_id`,`quest_progress`,`groupid`,`tickets_answered`,`respectstimestamp`,`forum_posts` FROM `user_stats` WHERE `id` = @user_id LIMIT 1");
                dbClient.AddParameter("user_id", Id);
                using (var otherReader = dbClient.ExecuteReader())
                    if (!otherReader.HasRows)
                        hasStats = false;

                if (!hasStats)
                    using (IQueryAdapter newDbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                        newDbClient.RunQuery("INSERT INTO `user_stats` (`id`) VALUES ('" + Id + "')");

                dbClient.SetQuery("SELECT `id`,`roomvisits`,`onlinetime`,`respect`,`respectgiven`,`giftsgiven`,`giftsreceived`,`dailyrespectpoints`,`dailypetrespectpoints`,`achievementscore`,`quest_id`,`quest_progress`,`groupid`,`tickets_answered`,`respectstimestamp`,`forum_posts` FROM `user_stats` WHERE `id` = @user_id LIMIT 1");
                dbClient.AddParameter("user_id", Id);
                using (var dataReader = dbClient.ExecuteReader())
                {
                    if (dataReader.Read())
                    {
                        this.Stats = new HabboStats(dataReader.GetInt32("roomvisits"), dataReader.GetDouble("onlineTime"), dataReader.GetInt32("respect"), dataReader.GetInt32("respectGiven"), dataReader.GetInt32("giftsGiven"),
                            dataReader.GetInt32("giftsReceived"), dataReader.GetInt32("dailyRespectPoints"), dataReader.GetInt32("dailyPetRespectPoints"), dataReader.GetInt32("AchievementScore"),
                            dataReader.GetInt32("quest_id"), dataReader.GetInt32("quest_progress"), dataReader.GetInt32("groupid"), dataReader.GetString("respectsTimestamp"), dataReader.GetInt32("forum_posts"));

                        if (dataReader.GetString("respectsTimestamp") != DateTime.Today.ToString("MM/dd"))
                        {
                            this.Stats.RespectsTimestamp = DateTime.Today.ToString("MM/dd");
                            SubscriptionData SubData = null;

                            int DailyRespects = 10;

                            if (this.Permissions.HasRight("mod_tool"))
                                DailyRespects = 20;
                            else if (PlusEnvironment.Game.SubscriptionManager.TryGetSubscriptionData(VIPRank, out SubData))
                                DailyRespects = SubData.Respects;

                            this.Stats.DailyRespectPoints = DailyRespects;
                            this.Stats.DailyPetRespectPoints = DailyRespects;

                            using (IQueryAdapter newDbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                            {
                                newDbClient.RunQuery("UPDATE `user_stats` SET `dailyRespectPoints` = '" + DailyRespects + "', `dailyPetRespectPoints` = '" + DailyRespects + "', `respectsTimestamp` = '" + DateTime.Today.ToString("MM/dd") + "' WHERE `id` = '" + Id + "' LIMIT 1");
                            }
                        }
                    }
                }
            }
            if (!PlusEnvironment.Game.GroupManager.TryGetGroup(this.Stats.FavouriteGroupId, out Group G))
                this.Stats.FavouriteGroupId = 0;
            #endregion
        }

        public bool InRoom
        {
            get
            {
                return CurrentRoomId >= 1 && CurrentRoom != null;
            }
        }

        public Room CurrentRoom
        {
            get
            {
                if (CurrentRoomId <= 0)
                    return null;

                if (PlusEnvironment.Game.RoomManager.TryGetRoom(CurrentRoomId, out Room _room))
                    return _room;

                return null;
            }
        }

        public bool CacheExpired()
        {
            TimeSpan Span = DateTime.Now - _timeCached;
            return (Span.TotalMinutes >= 30);
        }

        public string GetQueryString
        {
            get
            {
                this._habboSaved = true;
                return "UPDATE `users` SET `online` = '0', `last_online` = '" + PlusEnvironment.GetUnixTimestamp() + "', `activity_points` = '" + this.Duckets + "', `credits` = '" + this.Credits + "', `vip_points` = '" + this.Diamonds + "', `home_room` = '" + this.HomeRoom + "', `gotw_points` = '" + this.GOTWPoints + "', `time_muted` = '" + this.TimeMuted + "',`friend_bar_state` = '" + FriendBarStateUtility.GetInt(this.FriendbarState) + "' WHERE id = '" + Id + "' LIMIT 1;" +
                       "UPDATE `user_stats` SET `roomvisits` = '" + this.Stats.RoomVisits + "', `onlineTime` = '" + (PlusEnvironment.GetUnixTimestamp() - SessionStart + this.Stats.OnlineTime) + "', `respect` = '" + this.Stats.Respect + "', `respectGiven` = '" + this.Stats.RespectGiven + "', `giftsGiven` = '" + this.Stats.GiftsGiven + "', `giftsReceived` = '" + this.Stats.GiftsReceived + "', `dailyRespectPoints` = '" + this.Stats.DailyRespectPoints + "', `dailyPetRespectPoints` = '" + this.Stats.DailyPetRespectPoints + "', `AchievementScore` = '" + this.Stats.AchievementPoints + "', `quest_id` = '" + this.Stats.QuestID + "', `quest_progress` = '" + this.Stats.QuestProgress + "', `groupid` = '" + this.Stats.FavouriteGroupId + "',`forum_posts` = '" + this.Stats.ForumPosts + "' WHERE `id` = '" + this.Id + "' LIMIT 1;";
            }
        }

        public bool InitProcess()
        {
            this._process = new ProcessComponent();

            return this._process.Init(this);
        }

        public bool InitSearches()
        {
            this.NavigatorSearches = new SearchesComponent();

            return this.NavigatorSearches.Init(this);
        }

        public bool InitFX()
        {
            this.Effects = new EffectsComponent();

            return this.Effects.Init(this);
        }

        public bool InitClothing()
        {
            this.Clothing = new ClothingComponent();

            return this.Clothing.Init(this);
        }

        public bool InitIgnores()
        {
            this.Ignores = new IgnoresComponent();

            return this.Ignores.Init(this);
        }

        private bool InitPermissions()
        {
            this.Permissions = new PermissionComponent();

            return this.Permissions.Init(this);
        }

        public void InitInformation(UserData.UserData data)
        {
            BadgeComponent = new BadgeComponent(this   , data);
            Relationships = data.Relations;
        }

        public void Init(GameClient client, UserData.UserData data)
        {
            this.Achievements = data.achievements;

            this.FavoriteRooms = new ArrayList();
            foreach (int id in data.favouritedRooms)
            {
                FavoriteRooms.Add(id);
            }

            this._client = client;
            BadgeComponent = new BadgeComponent(this, data);
            InventoryComponent = new InventoryComponent(Id, client);

            Quests = data.quests;

            Messenger = new HabboMessenger(Id);
            Messenger.Init(data.friends, data.requests);
            this.FriendCount = Convert.ToInt32(data.friends.Count);
            this._disconnected = false;
            UsersRooms = data.rooms;
            Relationships = data.Relations;

            this.InitSearches();
            this.InitFX();
            this.InitClothing();
            this.InitIgnores();
        }

        public void OnDisconnect()
        {
            if (this._disconnected)
                return;

            try
            {
                if (this._process != null)
                    this._process.Dispose();
            }
            catch { }

            this._disconnected = true;

            PlusEnvironment.Game.ClientManager.UnregisterClient(Id, Username);

            if (!this._habboSaved)
            {
                this._habboSaved = true;
                using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                {
                    dbClient.RunQuery("UPDATE `users` SET `online` = '0', `last_online` = '" + PlusEnvironment.GetUnixTimestamp() + "', `activity_points` = '" + this.Duckets + "', `credits` = '" + this.Credits + "', `vip_points` = '" + this.Diamonds + "', `home_room` = '" + this.HomeRoom + "', `gotw_points` = '" + this.GOTWPoints + "', `time_muted` = '" + this.TimeMuted + "',`friend_bar_state` = '" + FriendBarStateUtility.GetInt(this.FriendbarState) + "' WHERE id = '" + Id + "' LIMIT 1;UPDATE `user_stats` SET `roomvisits` = '" + this.Stats.RoomVisits + "', `onlineTime` = '" + (PlusEnvironment.GetUnixTimestamp() - this.SessionStart + this.Stats.OnlineTime) + "', `respect` = '" + this.Stats.Respect + "', `respectGiven` = '" + this.Stats.RespectGiven + "', `giftsGiven` = '" + this.Stats.GiftsGiven + "', `giftsReceived` = '" + this.Stats.GiftsReceived + "', `dailyRespectPoints` = '" + this.Stats.DailyRespectPoints + "', `dailyPetRespectPoints` = '" + this.Stats.DailyPetRespectPoints + "', `AchievementScore` = '" + this.Stats.AchievementPoints + "', `quest_id` = '" + this.Stats.QuestID + "', `quest_progress` = '" + this.Stats.QuestProgress + "', `groupid` = '" + this.Stats.FavouriteGroupId + "',`forum_posts` = '" + this.Stats.ForumPosts +"' WHERE `id` = '" + this.Id + "' LIMIT 1;");

                    if (Permissions.HasRight("mod_tickets"))
                        dbClient.RunQuery("UPDATE `moderation_tickets` SET `status` = 'open', `moderator_id` = '0' WHERE `status` ='picked' AND `moderator_id` = '" + Id + "'");
                }
            }

            this.Dispose();

            this._client = null;

        }

        public void Dispose()
        {
            if (this.InventoryComponent != null)
                this.InventoryComponent.SetIdleState();

            if (this.UsersRooms != null)
                UsersRooms.Clear();

            if (this.InRoom && this.CurrentRoom != null)
                this.CurrentRoom.GetRoomUserManager().RemoveUserFromRoom(this._client, false, false);

            if (Messenger != null)
            {
                this.Messenger.AppearOffline = true;
                this.Messenger.Destroy();
            }

            if (this.Effects != null)
                this.Effects.Dispose();

            if (this.Clothing != null)
                this.Clothing.Dispose();

            if (this.Permissions != null)
                this.Permissions.Dispose();

            if (this.Ignores != null)
                this.Permissions.Dispose();
        }

        public void CheckCreditsTimer()
        {
            try
            {
                this.CreditsUpdateTick--;

                if (this.CreditsUpdateTick <= 0)
                {
                    int CreditUpdate = Convert.ToInt32(PlusEnvironment.SettingsManager.TryGetValue("user.currency_scheduler.credit_reward"));
                    int DucketUpdate = Convert.ToInt32(PlusEnvironment.SettingsManager.TryGetValue("user.currency_scheduler.ducket_reward"));

                    if (PlusEnvironment.Game.SubscriptionManager.TryGetSubscriptionData(this.VIPRank, out SubscriptionData SubData))
                    {
                        CreditUpdate += SubData.Credits;
                        DucketUpdate += SubData.Duckets;
                    }

                    this.Credits += CreditUpdate;
                    this.Duckets += DucketUpdate;

                    this._client.SendPacket(new CreditBalanceComposer(this.Credits));
                    this._client.SendPacket(new HabboActivityPointNotificationComposer(this.Duckets, DucketUpdate));

                    this.CreditsUpdateTick = Convert.ToInt32(PlusEnvironment.SettingsManager.TryGetValue("user.currency_scheduler.tick"));
                }
            }
            catch { }
        }

        public GameClient GetClient()
        {
            if (this._client != null)
                return this._client;

            return PlusEnvironment.Game.ClientManager.GetClientByUserID(Id);
        }

        public int GetQuestProgress(int p)
        {
            Quests.TryGetValue(p, out int progress);
            return progress;
        }

        public UserAchievement GetAchievementData(string p)
        {
            Achievements.TryGetValue(p, out UserAchievement achievement);
            return achievement;
        }

        public void ChangeName(string Username)
        {
            this.LastNameChange = PlusEnvironment.GetUnixTimestamp();
            this.Username = Username;

            this.SaveKey("username", Username);
            this.SaveKey("last_change", this.LastNameChange.ToString());
        }

        public void SaveKey(string Key, string Value)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("UPDATE `users` SET " + Key + " = @value WHERE `id` = '" + this.Id + "' LIMIT 1;");
                dbClient.AddParameter("value", Value);
                dbClient.RunQuery();
            }
        }

        public void PrepareRoom(int Id, string Password)
        {
            if (GetClient() == null)
                return;

            if (InRoom)
            {
                if (!PlusEnvironment.Game.RoomManager.TryGetRoom(CurrentRoomId, out Room OldRoom))
                    return;

                if (OldRoom.GetRoomUserManager() != null)
                    OldRoom.GetRoomUserManager().RemoveUserFromRoom(this.GetClient(), false, false);
            }

            if (IsTeleporting && TeleportingRoomID != Id)
            {
                this.GetClient().SendPacket(new CloseConnectionComposer());
                return;
            }

            Room Room = PlusEnvironment.Game.RoomManager.LoadRoom(Id);
            if (Room == null)
            {
                this.GetClient().SendPacket(new CloseConnectionComposer());
                return;
            }

            if (Room.isCrashed)
            {
                this.GetClient().SendNotification("This room has crashed! :(");
                this.GetClient().SendPacket(new CloseConnectionComposer());
                return;
            }

            CurrentRoomId = Room.RoomId;

            if (Room.GetRoomUserManager().userCount >= Room.UsersMax && !Permissions.HasRight("room_enter_full") && Id != Room.OwnerId)
            {
                this.GetClient().SendPacket(new CantConnectComposer(1));
                this.GetClient().SendPacket(new CloseConnectionComposer());
                return;
            }


            if (!this.Permissions.HasRight("room_ban_override") && Room.GetBans().IsBanned(this.Id))
            {
                this.RoomAuthOk = false;
                RoomAuthOk = false;
                this.GetClient().SendPacket(new CantConnectComposer(4));
                this.GetClient().SendPacket(new CloseConnectionComposer());
                return;
            }

            this.GetClient().SendPacket(new OpenConnectionComposer());
            if (!Room.CheckRights(this.GetClient(), true, true) && !IsTeleporting && !IsHopping)
            {
                if (Room.Access == RoomAccess.DOORBELL && !Permissions.HasRight("room_enter_locked"))
                {
                    if (Room.UserCount > 0)
                    {
                        this.GetClient().SendPacket(new DoorbellComposer(""));
                        Room.SendPacket(new DoorbellComposer(Username), true);
                        return;
                    }
                    else
                    {
                        this.GetClient().SendPacket(new FlatAccessDeniedComposer(""));
                        this.GetClient().SendPacket(new CloseConnectionComposer());
                        return;
                    }
                }
                else if (Room.Access == RoomAccess.PASSWORD && !Permissions.HasRight("room_enter_locked"))
                {
                    if (Password.ToLower() != Room.Password.ToLower() || String.IsNullOrWhiteSpace(Password))
                    {
                        this.GetClient().SendPacket(new GenericErrorComposer(-100002));
                        this.GetClient().SendPacket(new CloseConnectionComposer());
                        return;
                    }
                }
            }

            if (!EnterRoom(Room))
                this.GetClient().SendPacket(new CloseConnectionComposer());

        }

        public bool EnterRoom(Room Room)
        {
            if (Room == null)
                this.GetClient().SendPacket(new CloseConnectionComposer());

            this.GetClient().SendPacket(new RoomReadyComposer(Room.RoomId, Room.ModelName));
            if (Room.Wallpaper != "0.0")
                this.GetClient().SendPacket(new RoomPropertyComposer("wallpaper", Room.Wallpaper));
            if (Room.Floor != "0.0")
                this.GetClient().SendPacket(new RoomPropertyComposer("floor", Room.Floor));

            this.GetClient().SendPacket(new RoomPropertyComposer("landscape", Room.Landscape));
            this.GetClient().SendPacket(new RoomRatingComposer(Room.Score, !(RatedRooms.Contains(Room.RoomId) || Room.OwnerId == Id)));

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.RunQuery("INSERT INTO user_roomvisits (user_id,room_id,entry_timestamp,exit_timestamp,hour,minute) VALUES ('" + Id + "','" + CurrentRoomId + "','" + PlusEnvironment.GetUnixTimestamp() + "','0','" + DateTime.Now.Hour + "','" + DateTime.Now.Minute + "');");// +
            }


            if (Room.OwnerId != this.Id)
            {
                Stats.RoomVisits += 1;
                PlusEnvironment.Game.AchievementManager.ProgressAchievement(this.GetClient(), "ACH_RoomEntry", 1);
            }
            return true;
        }
    }
}