﻿using System.Collections.Generic;
using System.Collections.Concurrent;
using Plus.Database.Interfaces;


namespace Plus.HabboHotel.Users.Navigator.SavedSearches
{
    public class SearchesComponent
    {
        private ConcurrentDictionary<int, SavedSearch> _savedSearches;

        public SearchesComponent()
        {
            this._savedSearches = new ConcurrentDictionary<int, SavedSearch>();
        }

        public bool Init(Habbo Player)
        {
            if (this._savedSearches.Count > 0)
                this._savedSearches.Clear();
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`filter`,`search_code` FROM `user_saved_searches` WHERE `user_id` = @UserId");
                dbClient.AddParameter("UserId", Player.Id);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _savedSearches.TryAdd(reader.GetInt32("id"), new SavedSearch(reader.GetInt32("id"), reader.GetString("filter"), reader.GetString("search_code")));
            }
            return true;
        }

        public ICollection<SavedSearch> Searches
        {
            get { return this._savedSearches.Values; }
        }

        public bool TryAdd(int Id, SavedSearch Search)
        {
            return this._savedSearches.TryAdd(Id, Search);
        }

        public bool TryRemove(int Id, out SavedSearch Removed)
        {
            return this._savedSearches.TryRemove(Id, out Removed);
        }
    }
}
