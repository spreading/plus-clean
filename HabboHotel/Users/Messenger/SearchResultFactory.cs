﻿using System.Collections.Generic;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Users.Messenger
{
    public class SearchResultFactory
    {
        public static List<SearchResult> GetSearchResult(string query)
        {
            List<SearchResult> results = new List<SearchResult>();
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`username`,`motto`,`look`,`last_online` FROM users WHERE username LIKE @query LIMIT 50");

                dbClient.AddParameter("query", query + "%");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        results.Add(new SearchResult(reader.GetInt32("id"), reader.GetString("username"), reader.GetString("motto"), reader.GetString("look"), reader.GetString("last_online")));
            }

            return results;
        }
    }
}