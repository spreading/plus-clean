﻿using System.Collections.Generic;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.AI;

namespace Plus.HabboHotel.Users.Inventory.Pets
{
    static class PetLoader
    {
        public static List<Pet> GetPetsForUser(int UserId)
        {
            List<Pet> pets = new List<Pet>();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`user_id`,`room_id`,`name`,`x`,`y`,`z` FROM `bots` WHERE `user_id` = '" + UserId + "' AND `room_id` = '0' AND `ai_type` = 'pet'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        dbClient.SetQuery("SELECT `type`,`race`,`color`,`experience`,`energy`,`nutrition`,`respect`,`createstamp`,`have_saddle`,`anyone_ride`,`hairdye`,`pethair`,`gnome_clothing` FROM `bots_petdata` WHERE `id` = '" + reader.GetInt32("id") + "' LIMIT 1");

                        using (var newReader = dbClient.ExecuteReader())
                            if (newReader.Read())
                                pets.Add(new Pet(reader.GetInt32("id"), reader.GetInt32("user_id"), reader.GetInt32("room_id"), reader.GetString("name"), newReader.GetInt32("type"), newReader.GetString("race"), newReader.GetString("color"),
                                   newReader.GetInt32("experience"), newReader.GetInt32("energy"), newReader.GetInt32("nutrition"), newReader.GetInt32("respect"), newReader.GetDouble("createstamp"), reader.GetInt32("x"), reader.GetInt32("y"),
                                    reader.GetDouble("z"), newReader.GetInt32("have_saddle"), newReader.GetInt32("anyone_ride"), newReader.GetInt32("hairdye"), newReader.GetInt32("pethair"), newReader.GetString("gnome_clothing")));
                    }
            }
            return pets;
        }
    }
}