﻿using System.Collections.Generic;
using Plus.Database.Interfaces;


namespace Plus.HabboHotel.Users.Inventory.Bots
{
    class BotLoader
    {
        public static List<Bot> GetBotsForUser(int UserId)
        {
            List<Bot> bots = new List<Bot>();
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`user_id`,`name`,`motto`,`look`,`gender`FROM `bots` WHERE `user_id` = '" + UserId + "' AND `room_id` = '0' AND `ai_type` != 'pet'");

                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        bots.Add(new Bot(reader.GetInt32("id"), reader.GetInt32("user_id"), reader.GetString("name"),
                        reader.GetString("motto"), reader.GetString("look"), reader.GetString("gender")));
            }

            return bots;
        }
    }
}