﻿using System.Collections.Generic;

using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Users.Ignores
{
    public sealed class IgnoresComponent
    {
        private readonly List<int> _ignoredUsers;

        public IgnoresComponent()
        {
            this._ignoredUsers = new List<int>();
        }

        public bool Init(Habbo player)
        {
            if (this._ignoredUsers.Count > 0)
                return false;
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `user_ignores` WHERE `user_id` = @uid;");
                dbClient.AddParameter("uid", player.Id);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _ignoredUsers.Add(reader.GetInt32("ignore_id"));
            }
            return true;
        }

        public bool TryGet(int userId)
        {
            return this._ignoredUsers.Contains(userId);
        }

        public bool TryAdd(int userId)
        {
            if (this._ignoredUsers.Contains(userId))
                return false;

            this._ignoredUsers.Add(userId);
            return true;
        }

        public bool TryRemove(int userId)
        {
            return this._ignoredUsers.Remove(userId);
        }

        public ICollection<int> IgnoredUserIds()
        {
           return this._ignoredUsers;
        }

        public void Dispose()
        {
            this._ignoredUsers.Clear();
        }
    }
}