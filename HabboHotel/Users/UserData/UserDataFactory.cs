﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Users.Badges;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;

namespace Plus.HabboHotel.Users.UserData
{
    public class UserDataFactory
    {
        public static UserData GetUserData(string SessionTicket, out byte errorCode)
        {
            int UserId;

            ConcurrentDictionary<string, UserAchievement> Achievements = new ConcurrentDictionary<string, UserAchievement>();
            List<int> favouritedRooms = new List<int>();
            List<Badge> badges = new List<Badge>();
            Dictionary<int, MessengerBuddy> friends = new Dictionary<int, MessengerBuddy>();
            Dictionary<int, MessengerRequest> requests = new Dictionary<int, MessengerRequest>();
            List<RoomData> rooms = new List<RoomData>();
            Dictionary<int, int> quests = new Dictionary<int, int>();
            Dictionary<int, Relationship> Relationships = new Dictionary<int, Relationship>();
            Habbo habbo = null;

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`username`,`rank`,`motto`,`look`,`gender`,`last_online`,`credits`,`activity_points`,`home_room`,`block_newfriends`,`hide_online`,`hide_inroom`,`vip`,`account_created`,`vip_points`,`machine_id`,`volume`,`chat_preference`,`focus_preference`, `pets_muted`,`bots_muted`,`advertising_report_blocked`,`last_change`,`gotw_points`,`ignore_invites`,`time_muted`,`allow_gifts`,`friend_bar_state`,`disable_forced_effects`,`allow_mimic`,`rank_vip` FROM `users` WHERE `auth_ticket` = @sso LIMIT 1");
                dbClient.AddParameter("sso", SessionTicket);

                using (var reader = dbClient.ExecuteReader())
                    if (reader.Read())
                    {
                        UserId = reader.GetInt32("id");
                        if (PlusEnvironment.Game.ClientManager.GetClientByUserID(UserId) != null)
                        {
                            errorCode = 2;
                            PlusEnvironment.Game.ClientManager.GetClientByUserID(UserId).Disconnect();
                            return null;
                        }
                    }
                    else
                    {
                        errorCode = 1;
                        return null;
                    }

                dbClient.SetQuery("SELECT `group`,`level`,`progress` FROM `user_achievements` WHERE `userid` = '" + UserId + "'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        Achievements.TryAdd(reader.GetString("group"), new UserAchievement(reader.GetString("group"), reader.GetInt32("level"), reader.GetInt32("progress")));

                dbClient.SetQuery("SELECT room_id FROM user_favorites WHERE `user_id` = '" + UserId + "'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        favouritedRooms.Add(reader.GetInt32("room_id"));

                dbClient.SetQuery("SELECT `badge_id`,`badge_slot` FROM user_badges WHERE `user_id` = '" + UserId + "'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        badges.Add(new Badge(reader.GetString("badge_id"), reader.GetInt32("badge_slot")));

                dbClient.SetQuery(
                    "SELECT users.id,users.username,users.motto,users.look,users.last_online,users.hide_inroom,users.hide_online " +
                    "FROM users " +
                    "JOIN messenger_friendships " +
                    "ON users.id = messenger_friendships.user_one_id " +
                    "WHERE messenger_friendships.user_two_id = " + UserId + " " +
                    "UNION ALL " +
                    "SELECT users.id,users.username,users.motto,users.look,users.last_online,users.hide_inroom,users.hide_online " +
                    "FROM users " +
                    "JOIN messenger_friendships " +
                    "ON users.id = messenger_friendships.user_two_id " +
                    "WHERE messenger_friendships.user_one_id = " + UserId);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int friendID = reader.GetInt32("id");
                        string friendName = reader.GetString("username");
                        string friendLook = reader.GetString("look");
                        string friendMotto = reader.GetString("motto");
                        int friendLastOnline = reader.GetInt32("last_online");
                        bool friendHideOnline = PlusEnvironment.EnumToBool(reader.GetString("hide_online"));
                        bool friendHideRoom = PlusEnvironment.EnumToBool(reader.GetString("hide_inroom"));

                        if (friendID == UserId)
                            continue;

                        if (!friends.ContainsKey(friendID))
                            friends.Add(friendID, new MessengerBuddy(friendID, friendName, friendLook, friendMotto, friendLastOnline, friendHideOnline, friendHideRoom));
                    }

                dbClient.SetQuery("SELECT messenger_requests.from_id,messenger_requests.to_id,users.username FROM users JOIN messenger_requests ON users.id = messenger_requests.from_id WHERE messenger_requests.to_id = " + UserId);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int receiverID = reader.GetInt32("from_id");
                        int senderID = reader.GetInt32("to_id");

                        string requestUsername = reader.GetString("username");

                        if (receiverID != UserId)
                        {
                            if (!requests.ContainsKey(receiverID))
                                requests.Add(receiverID, new MessengerRequest(UserId, receiverID, requestUsername));
                        }
                        else
                        {
                            if (!requests.ContainsKey(senderID))
                                requests.Add(senderID, new MessengerRequest(UserId, senderID, requestUsername));
                        }
                    }

                dbClient.SetQuery("SELECT * FROM rooms WHERE `owner` = '" + UserId + "' LIMIT 150");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        rooms.Add(PlusEnvironment.Game.RoomManager.FetchRoomData(reader.GetInt32("id"), reader));

                dbClient.SetQuery("SELECT `quest_id`,`progress` FROM user_quests WHERE `user_id` = '" + UserId + "'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int questId = reader.GetInt32("quest_id");

                        if (quests.ContainsKey(questId))
                            quests.Remove(questId);

                        quests.Add(questId, reader.GetInt32("progress"));
                    }

                dbClient.SetQuery("SELECT `id`,`user_id`,`target`,`type` FROM `user_relationships` WHERE `user_id` = '" + UserId + "'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        if (friends.ContainsKey(reader.GetInt32("target")))
                            Relationships.Add(reader.GetInt32("target"), new Relationship(reader.GetInt32("id"), reader.GetInt32("target"), reader.GetInt32("type")));

                bool hasUser = true;
                dbClient.SetQuery("SELECT * FROM `user_info` WHERE `user_id` = '" + UserId + "' LIMIT 1");
                using (var reader = dbClient.ExecuteReader())
                    if (!reader.Read())
                        hasUser = false;

                if (!hasUser)
                    dbClient.RunQuery("INSERT INTO `user_info` (`user_id`) VALUES ('" + UserId + "')");
                dbClient.RunQuery("UPDATE `users` SET `online` = '1' WHERE `id` = '" + UserId + "' LIMIT 1");
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT users.*, user_info.* FROM `users`  JOIN `user_info` ON `user_info`.`user_id` = `users`.id WHERE `auth_ticket` = @sso LIMIT 1");
                dbClient.AddParameter("sso", SessionTicket);
                using (var reader = dbClient.ExecuteReader())
                    if (reader.Read())
                        habbo = new Habbo(reader);
            }

            errorCode = 0;
            return new UserData(UserId, Achievements, favouritedRooms, badges, friends, requests, rooms, quests, habbo, Relationships);
        }

        public static UserData GetUserData(int UserId)
        {
            ConcurrentDictionary<string, UserAchievement> Achievements = new ConcurrentDictionary<string, UserAchievement>();
            List<int> FavouritedRooms = new List<int>();
            List<Badge> Badges = new List<Badge>();
            Dictionary<int, MessengerBuddy> Friends = new Dictionary<int, MessengerBuddy>();
            Dictionary<int, MessengerRequest> FriendRequests = new Dictionary<int, MessengerRequest>();
            List<RoomData> Rooms = new List<RoomData>();
            Dictionary<int, int> Quests = new Dictionary<int, int>();
            Dictionary<int, Relationship> Relationships = new Dictionary<int, Relationship>();
            Habbo user = null;

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`username`,`rank`,`motto`,`look`,`gender`,`last_online`,`credits`,`activity_points`,`home_room`,`block_newfriends`,`hide_online`,`hide_inroom`,`vip`,`account_created`,`vip_points`,`machine_id`,`volume`,`chat_preference`, `focus_preference`, `pets_muted`,`bots_muted`,`advertising_report_blocked`,`last_change`,`gotw_points`,`ignore_invites`,`time_muted`,`allow_gifts`,`friend_bar_state`,`disable_forced_effects`,`allow_mimic`,`rank_vip` FROM `users` WHERE `id` = @id LIMIT 1");
                dbClient.AddParameter("id", UserId);

                using (var reader = dbClient.ExecuteReader())
                    if (!reader.Read())
                        return null;


                PlusEnvironment.Game.ClientManager.LogClonesOut(Convert.ToInt32(UserId));

                if (PlusEnvironment.Game.ClientManager.GetClientByUserID(UserId) != null)
                    return null;


                dbClient.SetQuery("SELECT * FROM `user_info` WHERE `user_id` = '" + UserId + "' LIMIT 1");
                using (var reader = dbClient.ExecuteReader())
                    if (!reader.HasRows)
                    {
                        dbClient.RunQuery("INSERT INTO `user_info` (`user_id`) VALUES ('" + UserId + "')");
                    }

                dbClient.SetQuery("SELECT `id`,`target`,`type` FROM user_relationships WHERE user_id=@id");
                dbClient.AddParameter("id", UserId);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        if (!Relationships.ContainsKey(reader.GetInt32("id")))
                        {
                            Relationships.Add(reader.GetInt32("target"), new Relationship(reader.GetInt32("id"), reader.GetInt32("target"), reader.GetInt32("type")));
                        }

                dbClient.SetQuery("SELECT users.*, user_info.* FROM `users`  JOIN `user_info` ON `user_info`.`user_id` = `users`.id WHERE `auth_ticket` = @userId LIMIT 1");
                dbClient.AddParameter("userId", UserId);
                using (var reader = dbClient.ExecuteReader())
                    if (reader.Read())
                        user = new Habbo(reader);
            }
            
            return new UserData(UserId, Achievements, FavouritedRooms, Badges, Friends, FriendRequests, Rooms, Quests, user, Relationships);
        }
    }
}