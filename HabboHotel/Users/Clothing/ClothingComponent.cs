﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;

using Plus.HabboHotel.Users.Clothing.Parts;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Users.Clothing
{
     public sealed class ClothingComponent
    {
        private Habbo _habbo;

        /// <summary>
        /// Effects stored by ID > Effect.
        /// </summary>
        private readonly ConcurrentDictionary<int, ClothingParts> _allClothing = new ConcurrentDictionary<int, ClothingParts>();

        public ClothingComponent()
        {
        }

        /// <summary>
        /// Initializes the EffectsComponent.
        /// </summary>
        /// <param name="UserId"></param>
        public bool Init(Habbo Habbo)
        {
            if (_allClothing.Count > 0)
                return false;
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`part_id`,`part` FROM `user_clothing` WHERE `user_id` = @id;");
                dbClient.AddParameter("id", Habbo.Id);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        if (this._allClothing.TryAdd(reader.GetInt32("part_id"), new ClothingParts(reader.GetInt32("id"), reader.GetInt32("part_id"), reader.GetString("part"))))
                        {
                            //umm?
                        }
            }

            this._habbo = Habbo;
            return true;
        }

        public void AddClothing(string ClothingName, List<int> PartIds)
        {
            foreach (int PartId in PartIds.ToList())
            {
                if (!this._allClothing.ContainsKey(PartId))
                {
                    int NewId = 0;
                    using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
                    {
                        dbClient.SetQuery("INSERT INTO `user_clothing` (`user_id`,`part_id`,`part`) VALUES (@UserId, @PartId, @Part)");
                        dbClient.AddParameter("UserId", this._habbo.Id);
                        dbClient.AddParameter("PartId", PartId);
                        dbClient.AddParameter("Part", ClothingName);
                        NewId = Convert.ToInt32(dbClient.InsertQuery());
                    }

                    this._allClothing.TryAdd(PartId, new ClothingParts(NewId, PartId, ClothingName));
                }
            }
        }

        public bool TryGet(int PartId, out ClothingParts ClothingPart)
        {
            return this._allClothing.TryGetValue(PartId, out ClothingPart);
        }

        public ICollection<ClothingParts> GetClothingParts
        {
            get { return this._allClothing.Values; }
        }

        /// <summary>
        /// Disposes the ClothingComponent.
        /// </summary>
        public void Dispose()
        {
            this._allClothing.Clear();
        }
    }
}
