﻿using Plus.Database.Interfaces;
using Plus.HabboHotel.Users;
using System;
using System.Collections.Generic;

namespace Plus.HabboHotel.Groups
{
    public static class GroupDao
    {
        public static List<Group> GetGroupsForUser(int userId)
        {
            List<Group> Groups = new List<Group>();
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT g.id FROM `group_memberships` AS m RIGHT JOIN `groups` AS g ON m.group_id = g.id WHERE m.user_id = @user");
                dbClient.AddParameter("user", userId);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        if (PlusEnvironment.Game.GroupManager.TryGetGroup(reader.GetInt32("id"), out Group Group))
                            Groups.Add(Group);
                    }
            }
            return Groups;
        }

        public static void TryCreateGroup(Group Group, Habbo Player)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("INSERT INTO `groups` (`name`, `desc`, `badge`, `owner_id`, `created`, `room_id`, `state`, `colour1`, `colour2`, `admindeco`) VALUES (@name, @desc, @badge, @owner, UNIX_TIMESTAMP(), @room, '0', @colour1, @colour2, '0')");
                dbClient.AddParameter("name", Group.Name);
                dbClient.AddParameter("desc", Group.Description);
                dbClient.AddParameter("owner", Group.CreatorId);
                dbClient.AddParameter("badge", Group.Badge);
                dbClient.AddParameter("room", Group.RoomId);
                dbClient.AddParameter("colour1", Group.Colour1);
                dbClient.AddParameter("colour2", Group.Colour2);
                Group.Id = Convert.ToInt32(dbClient.InsertQuery());

                Group.AddMember(Player.Id);
                Group.MakeAdmin(Player.Id);
            }
        }

        public static void SetGroupRoom(int roomId, int groupId)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("UPDATE `rooms` SET `group_id` = @gid WHERE `id` = @rid LIMIT 1");
                dbClient.AddParameter("gid", groupId);
                dbClient.AddParameter("rid", roomId);
                dbClient.RunQuery();

                dbClient.RunQuery("DELETE FROM `room_rights` WHERE `room_id` = '" + roomId + "'");
            }
        }

        public static Group GetGroup(int groupId)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `groups` WHERE `id` = @id LIMIT 1");
                dbClient.AddParameter("id", groupId);
                using (var reader = dbClient.ExecuteReader())
                    if (reader.Read())
                    {
                        return new Group(
                            reader.GetInt32("id"), reader.GetString("name"), reader.GetString("desc"), reader.GetString("badge"), reader.GetInt32("room_id"), reader.GetInt32("owner_id"),
                            reader.GetInt32("created"), reader.GetInt32("state"), reader.GetInt32("colour1"), reader.GetInt32("colour2"), reader.GetInt32("admindeco"), reader.GetInt32("forum_enabled") == 1);
                    }
            }
            return null;
        }

        public static void InitGroups(GroupManager manager)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`type`,`firstvalue`,`secondvalue` FROM `groups_items` WHERE `enabled` = '1'");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        switch (reader.GetString("type"))
                        {
                            case "base":
                                manager.BadgeBases.Add(new GroupBadgeParts(reader.GetInt32("id"), reader.GetString("firstvalue"), reader.GetString("secondvalue")));
                                break;

                            case "symbol":
                                manager.BadgeSymbols.Add(new GroupBadgeParts(reader.GetInt32("id"), reader.GetString("firstvalue"), reader.GetString("secondvalue")));
                                break;

                            case "color":
                                manager.BadgeBaseColours.Add(new GroupColours(reader.GetInt32("id"), reader.GetString("firstvalue")));
                                break;

                            case "color2":
                                manager.SymbolColours.Add(reader.GetInt32("id"), new GroupColours(reader.GetInt32("id"), reader.GetString("firstvalue")));
                                break;

                            case "color3":
                                manager.BackgroundColors.Add(reader.GetInt32("id"), new GroupColours(reader.GetInt32("id"), reader.GetString("firstvalue")));
                                break;
                        }
                    }
            }
        }

        public static void InitMembers(Group group)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `user_id`, `rank` FROM `group_memberships` WHERE `group_id` = @id");
                dbClient.AddParameter("id", group);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int UserId = reader.GetInt32("user_id");
                        bool IsAdmin = reader.GetInt32("rank") != 0;

                        if (IsAdmin)
                        {
                            if (!group.GetAdministrators.Contains(UserId))
                                group.GetAdministrators.Add(UserId);
                        }
                        else
                        {
                            if (!group.GetMembers.Contains(UserId))
                                group.GetMembers.Add(UserId);
                        }
                    }

                dbClient.SetQuery("SELECT `user_id` FROM `group_requests` WHERE `group_id` = @id");
                dbClient.AddParameter("id", group.Id);
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int UserId = reader.GetInt32("user_id");

                        if (group.GetMembers.Contains(UserId) || group.GetAdministrators.Contains(UserId))
                        {
                            dbClient.RunQuery("DELETE FROM `group_requests` WHERE `group_id` = '" + group.Id + "' AND `user_id` = '" + UserId + "'");
                        }
                        else if (!group.GetRequests.Contains(UserId))
                        {
                            group.GetRequests.Add(UserId);
                        }
                    }
            }
        }

        public static void MakeAdmin(int groupId, int userId)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("UPDATE group_memberships SET `rank` = '1' WHERE `user_id` = @uid AND `group_id` = @gid LIMIT 1");
                dbClient.AddParameter("gid", groupId);
                dbClient.AddParameter("uid", userId);
                dbClient.RunQuery();
            }
        }

        public static void TakeAdmin(int groupId, int userId)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("UPDATE group_memberships SET `rank` = '0' WHERE user_id = @uid AND group_id = @gid");
                dbClient.AddParameter("gid", groupId);
                dbClient.AddParameter("uid", userId);
                dbClient.RunQuery();
            }
        }

        public static void AddMember(Group group, int userId)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                if (group.IsAdmin(userId))
                {
                    dbClient.SetQuery("UPDATE `group_memberships` SET `rank` = '0' WHERE user_id = @uid AND group_id = @gid");
                    group.GetAdministrators.Remove(userId);
                    group.GetMembers.Add(userId);
                }
                else if (group.GroupType == GroupType.LOCKED)
                {
                    dbClient.SetQuery("INSERT INTO `group_requests` (user_id, group_id) VALUES (@uid, @gid)");
                    group.GetRequests.Add(userId);
                }
                else
                {
                    dbClient.SetQuery("INSERT INTO `group_memberships` (user_id, group_id) VALUES (@uid, @gid)");
                    group.GetMembers.Add(userId);
                }

                dbClient.AddParameter("gid", group.Id);
                dbClient.AddParameter("uid", userId);
                dbClient.RunQuery();
            }
        }

        public static void DeleteMember(int groupId, int userId)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("DELETE FROM group_memberships WHERE user_id=@uid AND group_id=@gid LIMIT 1");
                dbClient.AddParameter("gid", groupId);
                dbClient.AddParameter("uid", userId);
                dbClient.RunQuery();
            }
        }

        public static void HandleRequest(int groupId, int userId, bool accepted)
        {
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                if (accepted)
                {
                    dbClient.SetQuery("INSERT INTO group_memberships (user_id, group_id) VALUES (@uid, @gid)");
                    dbClient.AddParameter("gid", groupId);
                    dbClient.AddParameter("uid", userId);
                    dbClient.RunQuery();
                }

                dbClient.SetQuery("DELETE FROM group_requests WHERE user_id=@uid AND group_id=@gid LIMIT 1");
                dbClient.AddParameter("gid", groupId);
                dbClient.AddParameter("uid", userId);
                dbClient.RunQuery();
            }
        }
    }
}