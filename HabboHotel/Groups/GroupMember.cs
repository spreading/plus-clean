﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Groups
{
    public class GroupMember
    {
        public int Id { get; }
        public string Username { get; }
        public string Look { get; }

        public GroupMember(int Id, string Username, string Look)
        {
            this.Id = Id;
            this.Username = Username;
            this.Look = Look;
        }
    }
}
