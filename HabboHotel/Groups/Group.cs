﻿using System.Collections.Generic;
using System.Linq;

namespace Plus.HabboHotel.Groups
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AdminOnlyDeco { get; set; }
        public string Badge { get; set; }
        public int CreateTime { get; }
        public int CreatorId { get; }
        public string Description { get; set; }
        public int RoomId { get; }
        public int Colour1 { get; set; }
        public int Colour2 { get; set; }
        public bool ForumEnabled { get; }
        public GroupType GroupType { get; set; }
        public bool HasForum;
        private List<int> _members;
        private List<int> _requests;
        private List<int> _administrators;

        public Group(int Id, string Name, string Description, string Badge, int RoomId, int Owner, int Time, int Type, int Colour1, int Colour2, int AdminOnlyDeco, bool HasForum)
        {
            this.Id = Id;
            this.Name = Name;
            this.Description = Description;
            this.RoomId = RoomId;
            this.Badge = Badge;
            this.CreateTime = Time;
            this.CreatorId = Owner;
            this.Colour1 = (Colour1 == 0) ? 1 : Colour1;
            this.Colour2 = (Colour2 == 0) ? 1 : Colour2;
            this.HasForum = HasForum;

            switch (Type)
            {
                case 0:
                    this.GroupType = GroupType.OPEN;
                    break;
                case 1:
                    this.GroupType = GroupType.LOCKED;
                    break;
                case 2:
                    this.GroupType = GroupType.PRIVATE;
                    break;
            }

            this.AdminOnlyDeco = AdminOnlyDeco;
            this.ForumEnabled = ForumEnabled;

            this._members = new List<int>();
            this._requests = new List<int>();
            this._administrators = new List<int>();

            GroupDao.InitMembers(this);
        }

        public List<int> GetMembers
        {
            get { return this._members.ToList(); }
        }

        public List<int> GetRequests
        {
            get { return this._requests.ToList(); }
        }

        public List<int> GetAdministrators
        {
            get { return this._administrators.ToList(); }
        }

        public List<int> GetAllMembers
        {
            get
            {
                List<int> Members = new List<int>(this._administrators.ToList());
                Members.AddRange(this._members.ToList());

                return Members;
            }
        }

        public int MemberCount
        {
            get { return this._members.Count + this._administrators.Count; }
        }

        public int RequestCount
        {
            get { return this._requests.Count; }
        }

        public bool IsMember(int Id)
        {
            return this._members.Contains(Id) || this._administrators.Contains(Id);
        }

        public bool IsAdmin(int Id)
        {
            return this._administrators.Contains(Id);
        }

        public bool HasRequest(int Id)
        {
            return this._requests.Contains(Id);
        }

        public void MakeAdmin(int id)
        {
            if (this._members.Contains(id))
                this._members.Remove(id);

            GroupDao.MakeAdmin(this.Id, id);

            if (!this._administrators.Contains(id))
                this._administrators.Add(id);
        }

        public void TakeAdmin(int userId)
        {
            if (!this._administrators.Contains(userId))
                return;

            GroupDao.TakeAdmin(Id, userId);

            this._administrators.Remove(userId);
            this._members.Add(userId);
        }

        public void AddMember(int Id)
        {
            if (this.IsMember(Id) || this.GroupType == GroupType.LOCKED && this._requests.Contains(Id))
                return;

            GroupDao.AddMember(this, Id);
        }

        public void DeleteMember(int id)
        {
            if (IsMember(id))
            {
                if (this._members.Contains(id))
                    this._members.Remove(id);
            }
            else if (IsAdmin(id))
            {
                if (this._administrators.Contains(id))
                    this._administrators.Remove(id);
            }
            else
                return;

            GroupDao.DeleteMember(Id, id);
        }

        public void HandleRequest(int Id, bool Accepted)
        {
            GroupDao.HandleRequest(this.Id, Id, Accepted);

            if (Accepted)
                this._members.Add(Id);

            if (this._requests.Contains(Id))
                this._requests.Remove(Id);
        }

        public void ClearRequests()
        {
            this._requests.Clear();
        }

        public void Dispose()
        {
            this._requests.Clear();
            this._members.Clear();
            this._administrators.Clear();
        }
    }
}