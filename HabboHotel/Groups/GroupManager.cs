﻿using System.Collections.Generic;
using System.Collections.Concurrent;
using Plus.HabboHotel.Users;
using log4net;

namespace Plus.HabboHotel.Groups
{
    public class GroupManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(GroupManager));

        private readonly object _groupLoadingSync;
        private readonly ConcurrentDictionary<int, Group> _groups;

        private readonly List<GroupBadgeParts> _bases;
        private readonly List<GroupBadgeParts> _symbols;
        private readonly List<GroupColours> _baseColours;
        private readonly Dictionary<int, GroupColours> _symbolColours;
        private readonly Dictionary<int, GroupColours> _backgroundColours;

        public GroupManager()
        {
            this._groupLoadingSync = new object();
            this._groups = new ConcurrentDictionary<int, Group>();

            this._bases = new List<GroupBadgeParts>();
            this._symbols = new List<GroupBadgeParts>();
            this._baseColours = new List<GroupColours>();
            this._symbolColours = new Dictionary<int, GroupColours>();
            this._backgroundColours = new Dictionary<int, GroupColours>();
        }

        public void Init()
        {
            _bases.Clear();
            _symbols.Clear();
            _baseColours.Clear();
            _symbolColours.Clear();
            _backgroundColours.Clear();

            GroupDao.InitGroups(this);
        }

        public bool TryGetGroup(int id, out Group Group)
        {
            Group = null;

            if (this._groups.ContainsKey(id))
                return this._groups.TryGetValue(id, out Group);

            lock (this._groupLoadingSync)
            {
                if (this._groups.ContainsKey(id))
                    return this._groups.TryGetValue(id, out Group);

                Group = GroupDao.GetGroup(id);
                if (Group != null)
                {
                    _groups.TryAdd(Group.Id, Group);
                    return true;
                }
            }
            Group = null;
            return false;
        }

        public bool TryCreateGroup(Habbo Player, string Name, string Description, int RoomId, string Badge, int Colour1, int Colour2, out Group Group)
        {
            Group = new Group(0, Name, Description, Badge, RoomId, Player.Id, (int)PlusEnvironment.GetUnixTimestamp(), 0, Colour1, Colour2, 0, false);
            if (string.IsNullOrWhiteSpace(Name) || string.IsNullOrWhiteSpace(Badge))
                return false;

            GroupDao.TryCreateGroup(Group, Player);

            if (!this._groups.TryAdd(Group.Id, Group))
                return false;
            else
            {
                GroupDao.SetGroupRoom(RoomId, Group.Id);
            }
            return true;
        }

        public string GetColourCode(int id, bool colourOne)
        {
            if (colourOne)
            {
                if (this._symbolColours.ContainsKey(id))
                {
                    return this._symbolColours[id].Colour;
                }
            }
            else
            {
                if (this._backgroundColours.ContainsKey(id))
                {
                    return this._backgroundColours[id].Colour;
                }
            }

            return "";
        }

        public void DeleteGroup(int id)
        {
            Group Group = null;
            if (this._groups.ContainsKey(id))
                this._groups.TryRemove(id, out Group);

            if (Group != null)
            {
                Group.Dispose();
            }
        }

        public List<Group> GetGroupsForUser(int userId)
        {
            return GroupDao.GetGroupsForUser(userId);
        }


        public ICollection<GroupBadgeParts> BadgeBases
        {
            get { return this._bases; }
        }

        public ICollection<GroupBadgeParts> BadgeSymbols
        {
            get { return this._symbols; }
        }

        public ICollection<GroupColours> BadgeBaseColours
        {
            get { return this._baseColours; }
        }

        public ICollection<GroupColours> BadgeSymbolColours
        {
            get { return this._symbolColours.Values; }
        }

        public Dictionary<int, GroupColours> SymbolColours {
            get {
                return this._symbolColours;
            }
        }

        public Dictionary<int, GroupColours> BackgroundColors {
            get {
                return this._backgroundColours;
            }
        }

        public ICollection<GroupColours> BadgeBackColours
        {
            get { return this._backgroundColours.Values; }
        }
    }
}