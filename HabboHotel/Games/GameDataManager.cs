﻿using System.Linq;
using System.Collections.Generic;
using log4net;

namespace Plus.HabboHotel.Games
{
    public class GameDataManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(GameDataManager));

        private readonly Dictionary<int, GameData> _games;

        public GameDataManager()
        {
            this._games = new Dictionary<int, GameData>();

            this.Init();
        }

        public void Init()
        {
            GameDataDao.LoadGameData(_games);
            log.Info("Game Data Manager -> LOADED");
        }

        public bool TryGetGame(int GameId, out GameData GameData)
        {
            if (this._games.TryGetValue(GameId, out GameData))
                return true;
            return false;
        }

        public int GetCount()
        {
            int GameCount = 0;
            foreach (GameData Game in this._games.Values.ToList())
            {
                if (Game.GameEnabled)
                    GameCount += 1;
            }
            return GameCount;
        }

        public ICollection<GameData> GameData
        {
            get
            {
                return this._games.Values;
            }
        }
    }
}