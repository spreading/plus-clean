﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Games
{
    public class GameData
    {
        public int GameId { get; }
        public string GameName { get; }
        public string ColourOne { get; }
        public string ColourTwo { get; }
        public string ResourcePath { get; }
        public string StringThree { get; }
        public string GameSWF { get; }
        public string GameAssets { get; }
        public string GameServerHost { get; }
        public string GameServerPort { get; }
        public string SocketPolicyPort { get; }
        public bool GameEnabled { get; }

        public GameData(int GameId, string GameName, string ColourOne, string ColourTwo, string ResourcePath, string StringThree, string GameSWF, string GameAssets, string GameServerHost, string GameServerPort, string SocketPolicyPort, Boolean GameEnabled)
        {
            this.GameId = GameId;
            this.GameName = GameName;
            this.ColourOne = ColourOne;
            this.ColourTwo = ColourTwo;
            this.ResourcePath = ResourcePath;
            this.StringThree = StringThree;
            this.GameSWF = GameSWF;
            this.GameAssets = GameAssets;
            this.GameServerHost = GameServerHost;
            this.GameServerPort = GameServerPort;
            this.SocketPolicyPort = SocketPolicyPort;
            this.GameEnabled = GameEnabled;
        }
    }
}
