﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Permissions
{
    class PermissionCommand
    {
        public string Command { get; }
        public int GroupId { get; }
        public int SubscriptionId { get; }

        public PermissionCommand(string Command, int GroupId, int SubscriptionId)
        {
            this.Command = Command;
            this.GroupId = GroupId;
            this.SubscriptionId = SubscriptionId;
        }
    }
}
