﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Permissions
{
    public class PermissionGroup
    {
        public string Name { get; }
        public string Description { get; }
        public string Badge { get; }

        public PermissionGroup(string Name, string Description, string Badge)
        {
            this.Name = Name;
            this.Description = Description;
            this.Badge = Badge;
        }
    }
}
