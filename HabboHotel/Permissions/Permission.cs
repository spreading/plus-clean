﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Permissions
{
    class Permission
    {
        public int Id { get; }
        public string PermissionName { get; }
        public string Description { get; }

        public Permission(int Id, string Name, string Description)
        {
            this.Id = Id;
            this.PermissionName = Name;
            this.Description = Description;
        }
    }
}
