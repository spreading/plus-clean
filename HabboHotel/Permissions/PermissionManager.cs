﻿using System.Linq;
using System.Collections.Generic;
using log4net;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Users;

namespace Plus.HabboHotel.Permissions
{
    public sealed class PermissionManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PermissionManager));

        private readonly Dictionary<int, Permission> Permissions = new Dictionary<int, Permission>();

        private readonly Dictionary<string, PermissionCommand> _commands = new Dictionary<string, PermissionCommand>();

        private readonly Dictionary<int, PermissionGroup> PermissionGroups = new Dictionary<int, PermissionGroup>();

        private readonly Dictionary<int, List<string>> PermissionGroupRights = new Dictionary<int, List<string>>();

        private readonly Dictionary<int, List<string>> PermissionSubscriptionRights = new Dictionary<int, List<string>>();

        public void Init()
        {
            this.Permissions.Clear();
            this._commands.Clear();
            this.PermissionGroups.Clear();
            this.PermissionGroupRights.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `permissions`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        Permissions.Add(reader.GetInt32("id"), new Permission(reader.GetInt32("id"), reader.GetString("permission"), reader.GetString("description")));
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `permissions_commands`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        this._commands.Add(reader.GetString("command"), new PermissionCommand(reader.GetString("command"), reader.GetInt32("group_id"), reader.GetInt32("subscription_id")));
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `permissions_groups`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        this.PermissionGroups.Add(reader.GetInt32("id"), new PermissionGroup(reader.GetString("name"), reader.GetString("description"), reader.GetString("badge_code")));
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `permissions_rights`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int GroupId = reader.GetInt32("group_id");
                        int PermissionId = reader.GetInt32("permission_id");

                        if (!this.PermissionGroups.ContainsKey(GroupId))
                        {
                            continue; // permission group does not exist
                        }


                        if (!this.Permissions.TryGetValue(PermissionId, out Permission Permission))
                        {
                            continue; // permission does not exist
                        }

                        if (PermissionGroupRights.ContainsKey(GroupId))
                        {
                            this.PermissionGroupRights[GroupId].Add(Permission.PermissionName);
                        }
                        else
                        {
                            List<string> RightsSet = new List<string>()
                                {
                                    Permission.PermissionName
                                };

                            this.PermissionGroupRights.Add(GroupId, RightsSet);
                        }

                    }
            }

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `permissions_subscriptions`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        int PermissionId = reader.GetInt32("permission_id");
                        int SubscriptionId = reader.GetInt32("subscription_id");

                        if (!this.Permissions.TryGetValue(PermissionId, out Permission Permission))
                            continue; // permission does not exist

                        if (this.PermissionSubscriptionRights.ContainsKey(SubscriptionId))
                        {
                            this.PermissionSubscriptionRights[SubscriptionId].Add(Permission.PermissionName);
                        }
                        else
                        {
                            List<string> RightsSet = new List<string>()
                                {
                                    Permission.PermissionName
                                };

                            this.PermissionSubscriptionRights.Add(SubscriptionId, RightsSet);
                        }
                    }

                log.Info("Loaded " + this.Permissions.Count + " permissions.");
                log.Info("Loaded " + this.PermissionGroups.Count + " permissions groups.");
                log.Info("Loaded " + this.PermissionGroupRights.Count + " permissions group rights.");
                log.Info("Loaded " + this.PermissionSubscriptionRights.Count + " permissions subscription rights.");
            }
        }

        public bool TryGetGroup(int Id, out PermissionGroup Group)
        {
            return PermissionGroups.TryGetValue(Id, out Group);
        }

        public List<string> GetPermissionsForPlayer(Habbo Player)
        {
            List<string> PermissionSet = new List<string>();

            if (this.PermissionGroupRights.TryGetValue(Player.Rank, out List<string> PermRights))
            {
                PermissionSet.AddRange(PermRights);
            }

            if (this.PermissionSubscriptionRights.TryGetValue(Player.VIPRank, out List<string> SubscriptionRights))
            {
                PermissionSet.AddRange(SubscriptionRights);
            }

            return PermissionSet;
        }

        public List<string> GetCommandsForPlayer(Habbo Player)
        {
            return this._commands.Where(x => Player.Rank >= x.Value.GroupId && Player.VIPRank >= x.Value.SubscriptionId).Select(x => x.Key).ToList();
        }
    }
}