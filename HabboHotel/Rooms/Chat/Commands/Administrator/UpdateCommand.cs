﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.Core;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Administrator
{
    class UpdateCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_update"; }
        }

        public string Parameters
        {
            get { return "%variable%"; }
        }

        public string Description
        {
            get { return "Reload a specific part of the hotel."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("You must inculde a thing to update, e.g. :update catalog");
                return;
            }

            string UpdateVariable = Params[1];
            switch (UpdateVariable.ToLower())
            {
                case "cata":
                case "catalog":
                case "catalogue":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_catalog"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_catalog' permission.");
                            break;
                        }

                        PlusEnvironment.Game.CatalogManager.Init(PlusEnvironment.Game.ItemManager);
                        PlusEnvironment.Game.ClientManager.SendPacket(new CatalogUpdatedComposer());
                        Session.SendWhisper("Catalogue successfully updated.");
                        break;
                    }

                case "items":
                case "furni":
                case "furniture":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_furni"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_furni' permission.");
                            break;
                        }

                        PlusEnvironment.Game.ItemManager.Init();
                        Session.SendWhisper("Items successfully updated.");
                        break;
                    }

                case "models":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_models"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_models' permission.");
                            break;
                        }

                        PlusEnvironment.Game.RoomManager.LoadModels();
                        Session.SendWhisper("Room models successfully updated.");
                        break;
                    }

                case "promotions":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_promotions"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_promotions' permission.");
                            break;
                        }

                        PlusEnvironment.Game.LandingManager.LoadPromotions();
                        Session.SendWhisper("Landing view promotions successfully updated.");
                        break;
                    }

                case "youtube":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_youtube"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_youtube' permission.");
                            break;
                        }

                        PlusEnvironment.Game.TelevisionManager.Init();
                        Session.SendWhisper("Youtube televisions playlist successfully updated.");
                        break;
                    }

                case "filter":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_filter"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_filter' permission.");
                            break;
                        }

                        PlusEnvironment.Game.ChatManager.GetFilter().Init();
                        Session.SendWhisper("Filter definitions successfully updated.");
                        break;
                    }

                case "navigator":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_navigator"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_navigator' permission.");
                            break;
                        }

                        PlusEnvironment.Game.NavigatorManager.Init();
                        Session.SendWhisper("Navigator items successfully updated.");
                        break;
                    }

                case "ranks":
                case "rights":
                case "permissions":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_rights"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_rights' permission.");
                            break;
                        }

                        PlusEnvironment.Game.PermissionManager.Init();

                        foreach (GameClient Client in PlusEnvironment.Game.ClientManager.GetClients.ToList())
                        {
                            if (Client == null || Client.GetHabbo() == null || Client.GetHabbo().Permissions == null)
                                continue;

                            Client.GetHabbo().Permissions.Init(Client.GetHabbo());
                        }

                        Session.SendWhisper("Rank definitions successfully updated.");
                        break;
                    }

                case "config":
                case "settings":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_configuration"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_configuration' permission.");
                            break;
                        }

                        PlusEnvironment.SettingsManager.Init();
                        Session.SendWhisper("Server configuration successfully updated.");
                        break;
                    }

                case "bans":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_bans"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_bans' permission.");
                            break;
                        }

                        PlusEnvironment.Game.ModerationManager.ReCacheBans();
                        Session.SendWhisper("Ban cache re-loaded.");
                        break;
                    }

                case "quests":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_quests"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_quests' permission.");
                            break;
                        }

                        PlusEnvironment.Game.QuestManager.Init();
                        Session.SendWhisper("Quest definitions successfully updated.");
                        break;
                    }

                case "achievements":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_achievements"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_achievements' permission.");
                            break;
                        }

                        PlusEnvironment.Game.AchievementManager.LoadAchievements();
                        Session.SendWhisper("Achievement definitions bans successfully updated.");
                        break;
                    }

                case "moderation":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_moderation"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_moderation' permission.");
                            break;
                        }

                        PlusEnvironment.Game.ModerationManager.Init();
                        PlusEnvironment.Game.ClientManager.ModAlert("Moderation presets have been updated. Please reload the client to view the new presets.");

                        Session.SendWhisper("Moderation configuration successfully updated.");
                        break;
                    }

                case "vouchers":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_vouchers"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_vouchers' permission.");
                            break;
                        }

                        PlusEnvironment.Game.CatalogManager.GetVoucherManager().Init();
                        Session.SendWhisper("Catalogue vouche cache successfully updated.");
                        break;
                    }

                case "gc":
                case "games":
                case "gamecenter":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_game_center"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_game_center' permission.");
                            break;
                        }

                        PlusEnvironment.Game.GameDataManager.Init();
                        Session.SendWhisper("Game Center cache successfully updated.");
                        break;
                    }

                case "pet_locale":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_pet_locale"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_pet_locale' permission.");
                            break;
                        }

                        PlusEnvironment.Game.ChatManager.GetPetLocale().Init();
                        Session.SendWhisper("Pet locale cache successfully updated.");
                        break;
                    }

                case "locale":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_locale"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_locale' permission.");
                            break;
                        }

                        PlusEnvironment.LanguageManager.Init();
                        Session.SendWhisper("Locale cache successfully updated.");
                        break;
                    }

                case "mutant":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_anti_mutant"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_anti_mutant' permission.");
                            break;
                        }

                        PlusEnvironment.FigureManager.Init();
                        Session.SendWhisper("FigureData manager successfully reloaded.");
                        break;
                    }

                case "bots":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_bots"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_bots' permission.");
                            break;
                        }

                        PlusEnvironment.Game.BotManager.Init();
                        Session.SendWhisper("Bot managaer successfully reloaded.");
                        break;
                    }

                case "rewards":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_rewards"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_rewards' permission.");
                            break;
                        }

                        PlusEnvironment.Game.RewardManager.Reload();
                        Session.SendWhisper("Rewards managaer successfully reloaded.");
                        break;
                    }

                case "chat_styles":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_chat_styles"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_chat_styles' permission.");
                            break;
                        }

                        PlusEnvironment.Game.ChatManager.GetChatStyles().Init();
                        Session.SendWhisper("Chat Styles successfully reloaded.");
                        break;
                    }

                case "badge_definitions":
                    {
                        if (!Session.GetHabbo().Permissions.HasCommand("command_update_badge_definitions"))
                        {
                            Session.SendWhisper("Oops, you do not have the 'command_update_badge_definitions' permission.");
                            break;
                        }

                        PlusEnvironment.Game.BadgeManager.Init();
                        Session.SendWhisper("Badge definitions successfully reloaded.");
                        break;
                    }

                default:
                    Session.SendWhisper("'" + UpdateVariable + "' is not a valid thing to reload.");
                    break;
            }
        }
    }
}
