﻿using System;
using System.Collections.Generic;
using log4net;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Rooms.Chat.Styles
{
    public sealed class ChatStyleManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatStyleManager));

        private readonly Dictionary<int, ChatStyle> _styles;

        public ChatStyleManager()
        {
            this._styles = new Dictionary<int, ChatStyle>();
        }

        public void Init()
        {
            if (this._styles.Count > 0)
                this._styles.Clear();
            
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `room_chat_styles`;");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        try
                        {
                            if (!this._styles.ContainsKey(reader.GetInt32("id")))
                                this._styles.Add(reader.GetInt32("id"), new ChatStyle(reader.GetInt32("id"), reader.GetString("name"), reader.GetString("required_right")));
                        }
                        catch (Exception ex)
                        {
                            log.Error("Unable to load ChatBubble for ID [" + reader.GetInt32("id") + "]", ex);
                        }
                    }
            }

            log.Info("Loaded " + this._styles.Count + " chat styles.");
        }

        public bool TryGetStyle(int Id, out ChatStyle Style)
        {
            return this._styles.TryGetValue(Id, out Style);
        }
    }
}