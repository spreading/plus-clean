﻿using System.Collections.Generic;
using Plus.Database.Interfaces;


namespace Plus.HabboHotel.Rooms.Chat.Pets.Commands
{
    public class PetCommandManager
    {
        private Dictionary<int, string> _commandRegister;
        private Dictionary<string, string> _commandDatabase;
        private Dictionary<string, PetCommand> _petCommands;

        public PetCommandManager()
        {
            this._petCommands = new Dictionary<string, PetCommand>();
            this._commandRegister = new Dictionary<int, string>();
            this._commandDatabase = new Dictionary<string, string>();

            this.Init();
        }

        public void Init()
        {
            this._petCommands.Clear();
            this._commandRegister.Clear();
            this._commandDatabase.Clear();

            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `bots_pet_commands`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                    {
                        _commandRegister.Add(reader.GetInt32("id"), reader.GetString("input_title"));
                        _commandDatabase.Add(reader.GetString("input_title") + ".input", reader.GetString("input"));
                    }
            }

            foreach (var pair in _commandRegister)
            {
                int commandID = pair.Key;
                string commandStringedID = pair.Value;
                string[] commandInput = this._commandDatabase[commandStringedID + ".input"].Split(',');

                foreach (string command in commandInput)
                {
                    this._petCommands.Add(command, new PetCommand(commandID, command));
                }
            }
        }

        public int TryInvoke(string Input)
        {
            if (this._petCommands.TryGetValue(Input.ToLower(), out PetCommand Command))
                return Command.Id;
            return 0;
        }
    }
}