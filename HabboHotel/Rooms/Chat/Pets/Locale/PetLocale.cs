﻿using System.Collections.Generic;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Rooms.Chat.Pets.Locale
{
    public class PetLocale
    {
        private Dictionary<string, string[]> _values;

        public PetLocale()
        {
            this._values = new Dictionary<string, string[]>();

            this.Init();
        }

        public void Init()
        {
            this._values = new Dictionary<string, string[]>();
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `bots_pet_responses`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _values.Add(reader.GetString("pet_id"), reader.GetString("responses").Split(';'));
            }
        }

        public string[] GetValue(string key)
        {
            if (this._values.TryGetValue(key, out string[] value))
                return value;
            return new[] { "Unknown pet speach:" + key };
        }
    }
}