﻿using System;
using System.Collections.Generic;
using System.Data;

using Plus.HabboHotel.Groups;
using Plus.Database.Interfaces;
using MySql.Data.MySqlClient;

namespace Plus.HabboHotel.Rooms
{
    public class RoomData
    {
        public int Id;
        public int AllowPets;
        public int AllowPetsEating;
        public int RoomBlockingEnabled;
        public int Category;
        public string Description;
        public string Floor;
        public int FloorThickness;
        public Group Group;
        public int Hidewall;
        public string Landscape;
        public string ModelName;
        public string Name;
        public string OwnerName;
        public int OwnerId;
        public string Password;
        public int Score;
        public RoomAccess Access;
        public List<string> Tags;
        public string Type;
        public int UsersMax;
        public int UsersNow;
        public int WallThickness;
        public string Wallpaper;
        public int WhoCanBan;
        public int WhoCanKick;
        public int WhoCanMute;
        private RoomModel mModel;
        public int chatMode;
        public int chatSpeed;
        public int chatSize;
        public int extraFlood;
        public int chatDistance;

        public int TradeSettings;//Default = 2;

        public RoomPromotion _promotion;

        public bool PushEnabled;
        public bool PullEnabled;
        public bool SPushEnabled;
        public bool SPullEnabled;
        public bool EnablesEnabled;
        public bool RespectNotificationsEnabled;
        public bool PetMorphsAllowed;

        public void Fill(MySqlDataReader reader)
        {
            Id = reader.GetInt32("id");
            Name = reader.GetString("caption");
            Description = reader.GetString("description");
            Type = reader.GetString("roomtype");
            OwnerId = reader.GetInt32("owner");

            OwnerName = "";
            using (IQueryAdapter dbClient = PlusEnvironment.DatabaseManager.GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `username` FROM `users` WHERE `id` = @owner LIMIT 1");
                dbClient.AddParameter("owner", OwnerId);
                string result = dbClient.GetString();
                if (!String.IsNullOrEmpty(result))
                    OwnerName = result;
            }

            this.Access = RoomAccessUtility.ToRoomAccess(reader["state"].ToString().ToLower());

            Category = reader.GetInt32("category");
            if (!string.IsNullOrEmpty(reader["users_now"].ToString()))
                UsersNow = reader.GetInt32("users_now");
            else
                UsersNow = 0;
            UsersMax = reader.GetInt32("users_max");
            ModelName = reader.GetString("model_name");
            Score = reader.GetInt32("score");
            Tags = new List<string>();
            AllowPets = reader.GetInt32("allow_pets");
            AllowPetsEating = reader.GetInt32("allow_pets_eat");
            RoomBlockingEnabled = reader.GetInt32("room_blocking_disabled");
            Hidewall = reader.GetInt32("allow_hidewall");
            Password = reader.GetString("password");
            Wallpaper = reader.GetString("wallpaper");
            Floor = reader.GetString("floor");
            Landscape = reader.GetString("landscape");
            FloorThickness = reader.GetInt32("floorthick");
            WallThickness = reader.GetInt32("wallthick");
            WhoCanMute = reader.GetInt32("mute_settings");
            WhoCanKick = reader.GetInt32("kick_settings");
            WhoCanBan = reader.GetInt32("ban_settings");
            chatMode = reader.GetInt32("chat_mode");
            chatSpeed = reader.GetInt32("chat_speed");
            chatSize = reader.GetInt32("chat_size");
            TradeSettings = reader.GetInt32("trade_settings");

            Group G = null;
            if (PlusEnvironment.Game.GroupManager.TryGetGroup(reader.GetInt32("group_id"), out G))
                Group = G;
            else
                Group = null;

            foreach (string Tag in reader["tags"].ToString().Split(','))
            {
                Tags.Add(Tag);
            }

            mModel = PlusEnvironment.Game.RoomManager.GetModel(ModelName);

            this.PushEnabled = PlusEnvironment.EnumToBool(reader["push_enabled"].ToString());
            this.PullEnabled = PlusEnvironment.EnumToBool(reader["pull_enabled"].ToString());
            this.SPushEnabled = PlusEnvironment.EnumToBool(reader["spush_enabled"].ToString());
            this.SPullEnabled = PlusEnvironment.EnumToBool(reader["spull_enabled"].ToString());
            this.EnablesEnabled = PlusEnvironment.EnumToBool(reader["enables_enabled"].ToString());
            this.RespectNotificationsEnabled = PlusEnvironment.EnumToBool(reader["respect_notifications_enabled"].ToString());
            this.PetMorphsAllowed = PlusEnvironment.EnumToBool(reader["pet_morphs_allowed"].ToString());
        }

        public RoomPromotion Promotion
        {
            get { return this._promotion; }
            set { this._promotion = value; }
        }

        public bool HasActivePromotion
        {
            get { return this.Promotion != null; }
        }

        public void EndPromotion()
        {
            if (!this.HasActivePromotion)
                return;

            this.Promotion = null;
        }

        public RoomModel Model
        {
            get
            {
                if (mModel == null)
                    mModel = PlusEnvironment.Game.RoomManager.GetModel(ModelName);
                return mModel;
            }
        }
    }
}