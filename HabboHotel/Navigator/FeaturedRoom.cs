﻿using System;

namespace Plus.HabboHotel.Navigator
{
    public class FeaturedRoom
    {
        public int RoomId { get; }
        public string Caption { get; }
        public string Description { get; }
        public string Image { get; }

        public FeaturedRoom(int RoomId, string Caption, string Description, string Image)
        {
            this.RoomId = RoomId;
            this.Caption = Caption;
            this.Description = Description;
            this.Image = Image;
        }
    }
}